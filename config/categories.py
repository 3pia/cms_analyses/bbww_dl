# coding: utf-8
# flake8: noqa

import utils.aci as aci


def setup_categories(cfg):

    cfg.aux["collate_cats"] = {
        ("_resolved_1b", "_resolved_2b", "_boosted_1b"): ("_incl", "_incl3"),
        (r"_resolved(?!_\db)", "_boosted_1b"): ("_incl", "_incl2"),
    }

    # ee = cfg.add_category(
    #     "ee",
    #     label="ee $\geq 1b$",
    #     label_short="ee >=1b",
    #     aux={"fit": False, "fit_variable": "jet1_pt"},
    # )

    # mumu = cfg.add_category(
    #     "mumu",
    #     label=r"$\mu\mu$ $\geq 1b$",
    #     label_short=r"$\mu\mu$ >=1b",
    #     aux={"fit": False, "fit_variable": "jet1_pt"},
    # )

    # emu = cfg.add_category(
    #     "emu",
    #     label=r"e$\mu$ $\geq 1b$",
    #     label_short=r"e$\mu$ >=1b",
    #     aux={"fit": False, "fit_variable": "jet1_pt"},
    # )

    # for t in ["resolved", "boosted", "inclusive"]:
    #     locals()[f"ee_{t}"] = cfg.add_category(
    #         f"ee_{t}",
    #         label=rf"ee {t} $\geq 1b$",
    #         label_short=rf"ee >=1b, {t}",
    #         aux={"fit": False, "fit_variable": "jet1_pt"},
    #     )
    #
    #     locals()[f"mumu_{t}"] = cfg.add_category(
    #         f"mumu_{t}",
    #         label=rf"$\mu\mu$ {t} $\geq 1b$",
    #         label_short=rf"$\mu\mu$ >=1b, {t}",
    #         aux={"fit": False, "fit_variable": "jet1_pt"},
    #     )
    #
    #     locals()[f"emu_{t}"] = cfg.add_category(
    #         f"emu_{t}",
    #         label=rf"e$\mu$ {t} $\geq 1b$",
    #         label_short=rf"e$\mu$ >=1b, {t}",
    #         aux={"fit": False, "fit_variable": "jet1_pt"},
    #     )

    # dnn categories
    # use label for pretty printing
    # use label_short for latex
    get = cfg.processes.get
    multiclass_config = cfg.aux["multiclass"]
    multiclass_processes = multiclass_config.groups[multiclass_config.group]
    classes = {
        f"dnn_node_{cls}": get(cls).label_short if "HH" in cls else get(cls).label
        for cls in multiclass_processes.keys()
    }
    classes.update(
        grp_dy_vv="DY + Multiboson",
        grp_other_top="Top + Other",
        grp_other_notop="Others",
        grp_top="Top",
    )
    modes = {
        "incl": "inclusive",
        "inclusive": "inclusive (bad)",
        "boosted_1b": "boosted",
        "boosted_0b": "boosted 0b",
        "resolved": "resolved $\geq$ 1b",
        "resolved_1b": "resolved = 1b",
        "resolved_2b": "resolved $\geq$ 2b",
        "resolved_0b_to1b": "resolved 0b (1)",
        "resolved_0b_to2b": "resolved 0b (2)",
    }
    for ch, chtex in {"ee": "ee", "emu": "eμ", "mumu": "μμ", "all": "all"}.items():
        for mode, modetex in modes.items():
            nbase = f"{ch}_{mode}_sr"
            lbase = f"{chtex} {modetex}"
            cfg.categories.add(
                aci.Category(name=nbase, label=lbase, aux=dict(fit_variable="jet1_pt"))
            )
            for cls, ctex in classes.items():
                cfg.categories.add(
                    aci.Category(
                        name=f"{nbase}_{cls}",
                        label=f"{lbase}, {ctex}",
                        aux={"fit_variable": "dnn_score_max"},
                    )
                )

    for c in [
        "all_incl_sr_grp_dy_vv",
        "all_resolved_sr_grp_other_top",
        "all_boosted_1b_sr_grp_other_top",
    ] + [
        f"all_{region}_sr_dnn_node_{process}"
        for region in ["boosted_1b", "resolved_1b", "resolved_2b"]
        for process in [p for p, g in multiclass_processes.items() if "fit" in g["groups"]]
    ]:
        cfg.categories.get(c).tags = {"fit"}
        if "HH" in c:
            cfg.categories.get(c).tags |= {"signal"}
            if "GluGlu" in c:
                cfg.categories.get(c).tags |= {"ggF"}
            if "VBF" in c:
                cfg.categories.get(c).tags |= {"VBF"}
            if "boosted" in c:
                cfg.categories.get(c).tags |= {"boosted"}
            if "resolved_1b" in c:
                cfg.categories.get(c).tags |= {"resolved_1b"}
            if "resolved_2b" in c:
                cfg.categories.get(c).tags |= {"resolved_2b"}
        else:
            cfg.categories.get(c).tags |= {"background"}

    # categories for additional plots of variables
    for var in cfg.variables:
        if "has_category" in var.tags:
            cat_tags = {tag[4:] for tag in var.tags if tag.startswith("cat_")}
            cfg.categories.add(
                aci.Category(
                    name=f"all_incl_sr_{var.name}",
                    label=f"",
                    aux={"fit_variable": var.name},
                    tags=cat_tags,
                )
            )
