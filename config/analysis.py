# coding: utf-8
# flake8: noqa

"""
Definition of the diHiggs analysis in the bbWW (DL) channel.
"""

import re

#
# analysis and config
#
from tasks.corrections.gf_hh import HHReweigthing
from config.util import PrepareConfig
from utils.aci import rgb
from config.constants import (
    BR_HH_BBTAUTAU,
    BR_HH_BBVV_DL,
    BR_HH_BBWW_SL,
    HHres,
    HHxs,
    gfHHparams,
    gfHHparamsNoSample,
)
import utils.aci as aci
from config.analysis import analysis


from tasks.multiclass import MulticlassConfig

# create the analysis
analysis = analysis.copy(name="diHiggs_dl", label="Dilepton", label_short="bbWW (DL)")

get = analysis.processes.get

# add analysis specific processes
specific_processes = []

# fmt: off
dc_others = aci.Process(name="dc_others", id=999989, label=r"Others", color=rgb(103, 179, 153), processes=[get("rare")])
specific_processes.append(dc_others)

dc_ttVX = aci.Process(name="dc_ttVX", id=999990, label="ttVX", processes=[get("ttV"), get("ttVV")])
specific_processes.append(dc_ttVX)

dc_ggHbb = aci.Process(name="dc_ggHbb", id=23412341, label="ZH(bb)", processes=[get("GluGluHToBB_M125"), get("GluGluHToTauTau_M125")])
specific_processes.append(dc_ggHbb)

dc_qqHbb = aci.Process(name="dc_qqHbb", id=23412351, label="ggH(bb)", processes=[get("VBFHToBB_M_125"), get("VBFHToTauTau_M125")])
specific_processes.append(dc_qqHbb)

dc_ZHbb = aci.Process(name="dc_ZHbb", id=23412361, label="ZH(bb)", processes=[get("ZH_DY"), get("ggZH"), get("ZHToTauTau_M125")])
specific_processes.append(dc_ZHbb)

plot_vv_v = aci.Process(name="plot_vv(v)", id=999991, color=rgb(82, 124, 182), label=r"Multiboson", processes=[get("vv"), get("vvv")])
specific_processes.append(plot_vv_v)

plot_other = aci.Process(name="plot_other", id=999992, color=rgb(152, 78, 163), label=r"Others", processes=[dc_others, get("ttV"), get("ttVV")])
specific_processes.append(plot_other)

plot_H = aci.Process(name="plot_H", id=23412371, label=r"(SM) Higgs", color=rgb(120, 33, 80), processes=[get("ttVH"), get("HZJ_HToWW_M125"), get("TH"), get("ttH"), get("WH"), dc_ggHbb, dc_qqHbb, dc_ZHbb])
specific_processes.append(plot_H)

class_multiboson = aci.Process(name="class_multiboson", id=3789273, label=r"Multiboson", label_short="VV(V)", processes=[get("vv"), get("vvv")])
specific_processes.append(class_multiboson)

class_ttVX = aci.Process(name="class_ttVX", id=3789274, label=r"$t\bar{t}$ + VX", label_short="ttVX", processes=[get("ttV"), get("ttVV")])
specific_processes.append(class_ttVX)

# content is generated on-the-fly, don't whine about no datasets [dynamic]
fakes = aci.Process(name="fakes", id=999993, color=rgb(137, 207, 240), label="Fakes", is_data=True, aux=dict(dynamic=True))
specific_processes.append(fakes)

### DY estimation
# content is generated on-the-fly, don't whine about no datasets [dynamic]
DYest = aci.Process(name="DYest", id=999996, color=rgb(40, 53, 125), label="Drell-Yan (est.)", is_data=True, aux=dict(dynamic=True))
specific_processes.append(DYest)

# content is generated on-the-fly, don't whine about no datasets [dynamic]
DYestMC = aci.Process(name="DYestMC", id=999997, color=rgb(60, 53, 125), label="Drell-Yan (est., from MC)", is_data=True, aux=dict(dynamic=True))
specific_processes.append(DYestMC)

# content is generated on-the-fly, don't whine about no datasets [dynamic]
AllMC = aci.Process(name="AllMC", id=999998, color=rgb(80, 53, 125), label="AllMC", label_short="AllMC", is_data=False, aux=dict(dynamic=True))
specific_processes.append(AllMC)
###

class_rare = aci.Process(name="class_rare", id=3789275, label_short="rare", label=r"Rare", processes=[get("wjets"), get("rare")])
specific_processes.append(class_rare)

class_background = aci.Process(name="class_background", id=3789276, label="Background", processes=[get("H"), get("dy"), get("rare"), get("st"), get("tt"), get("ttV"), get("ttVV"), get("vv"), get("vvv"), get("wjets")])
specific_processes.append(class_background)

subcats_class_other = aci.Process(name="subcats_class_other", id=9989276, label="Other", processes=[get("rare"), get("st"), get("tt"), get("ttV"), get("ttVV"), get("wjets")])
specific_processes.append(subcats_class_other)

tll_class_other = aci.Process(name="tll_class_other", id=93789276, label="Other", processes=[get("H"), get("rare"), get("ttV"), get("ttVV"), get("vv"), get("vvv"), get("wjets")])
specific_processes.append(tll_class_other)

class_signal = aci.Process(name="class_signal", id=3789260, label="Signal", processes=[get("HH_2B2VTo2L2Nu_GluGlu_NLO"), get("HH_2B2VTo2L2Nu_VBF_NLO")])
specific_processes.append(class_signal)

class_top = aci.Process(name="class_top", id=7789276, label="Top", processes=[get("st"), get("tt"), get("ttV"), get("ttVV")])
specific_processes.append(class_top)

class_other = aci.Process(name="class_other", id=7789277, label="Other", processes=[get("wjets"), get("rare")])
specific_processes.append(class_other)

class_dy = aci.Process(name="class_dy", id=7789278, label="DY", processes=[get("dy"), get("vv"), get("vvv")])
specific_processes.append(class_dy)

class_HHGluGlu_NLO_SM = aci.Process(name="class_HHGluGlu_NLO_SM", id=27789277, label="HH(ggF) SM", processes=[get("ggHH_kl_1_kt_1_2B2VTo2L2Nu"), get("ggHH_kl_1_kt_1_2B2Tau")])
specific_processes.append(class_HHGluGlu_NLO_SM)

class_HHGluGlu_NLO_BSM = aci.Process(name="class_HHGluGlu_NLO_BSM", id=37789277, label="HH(ggF) BSM", processes=[get("ggHH_kl_0_kt_1_2B2VTo2L2Nu"), get("ggHH_kl_2p45_kt_1_2B2VTo2L2Nu"), get("ggHH_kl_5_kt_1_2B2VTo2L2Nu"), get("ggHH_kl_0_kt_1_2B2Tau"), get("ggHH_kl_2p45_kt_1_2B2Tau"), get("ggHH_kl_5_kt_1_2B2Tau")])
specific_processes.append(class_HHGluGlu_NLO_BSM)

class_HHVBF_NLO_SM = aci.Process(name="class_HHVBF_NLO_SM", id=27789278, label="HH(VBF) SM", processes=[get("qqHH_CV_1_C2V_1_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_1_kl_1_2B2Tau")])
specific_processes.append(class_HHVBF_NLO_SM)

class_HHVBF_NLO_BSM = aci.Process(name="class_HHVBF_NLO_BSM", id=37789278, label="HH(VBF) BSM", processes=[get("qqHH_CV_1_C2V_1_kl_0_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_1_kl_2_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_2_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_1_5_C2V_1_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_0_5_C2V_1_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_0_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_1_kl_0_2B2Tau"), get("qqHH_CV_1_C2V_1_kl_2_2B2Tau"), get("qqHH_CV_1_C2V_2_kl_1_2B2Tau"), get("qqHH_CV_1_5_C2V_1_kl_1_2B2Tau"), get("qqHH_CV_0_5_C2V_1_kl_1_2B2Tau"), get("qqHH_CV_1_C2V_0_kl_1_2B2Tau")])
specific_processes.append(class_HHVBF_NLO_BSM)

class_HHGluGlu_NLO = aci.Process(name="class_HHGluGlu_NLO", id=17789277, label="HH(ggF)", processes=[get("HH_2B2VTo2L2Nu_GluGlu_NLO"), get("HH_2B2Tau_GluGlu_NLO")])
specific_processes.append(class_HHGluGlu_NLO)

class_HHGluGlu = aci.Process(name="class_HHGluGlu", id=17789287, label="HH(ggF)", processes=[get("HH_2B2VTo2L2Nu_GluGlu"), get("HH_2B2Tau_GluGlu")])
specific_processes.append(class_HHGluGlu)

class_HHVBF_NLO = aci.Process(name="class_HHVBF_NLO", id=17789278, label="HH(VBF)", processes=[get("HH_2B2VTo2L2Nu_VBF_NLO"), get("HH_2B2Tau_VBF_NLO")])
specific_processes.append(class_HHVBF_NLO)

class_HHVBF = aci.Process(name="class_HHVBF", id=17789288, label="HH(VBF)", processes=[get("HH_2B2VTo2L2Nu_VBF"), get("HH_2B2Tau_VBF")])
specific_processes.append(class_HHVBF)

tll_class_hh = aci.Process(name="tll_class_hh", id=93789277, label="HH", processes=[get("GluGluToHHTo2B2VTo2L2Nu_node_SM"), get("GluGluToHHTo2B2Tau_node_SM"), get("qqHH_CV_1_C2V_1_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_1_kl_0_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_1_kl_2_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_2_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_1_5_C2V_1_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_0_5_C2V_1_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_0_kl_1_2B2VTo2L2Nu"), get("qqHH_CV_1_C2V_1_kl_1_2B2Tau"), get("qqHH_CV_1_C2V_1_kl_0_2B2Tau"), get("qqHH_CV_1_C2V_1_kl_2_2B2Tau"), get("qqHH_CV_1_C2V_2_kl_1_2B2Tau"), get("qqHH_CV_1_5_C2V_1_kl_1_2B2Tau"), get("qqHH_CV_0_5_C2V_1_kl_1_2B2Tau"), get("qqHH_CV_1_C2V_0_kl_1_2B2Tau")])
specific_processes.append(tll_class_hh)

# fmt: on

# merge signals
specific_signals = []
signals = (
    "qqHH_CV_1_C2V_1_kl_0",
    "qqHH_CV_1_C2V_1_kl_1",
    "qqHH_CV_1_C2V_1_kl_2",
    "qqHH_CV_1_C2V_2_kl_1",
    "qqHH_CV_1_5_C2V_1_kl_1",
    "qqHH_CV_0_5_C2V_1_kl_1",
    "qqHH_CV_1_C2V_0_kl_1",
    "ggHH_kl_0_kt_1",
    "ggHH_kl_1_kt_1",
    "ggHH_kl_2p45_kt_1",
    "ggHH_kl_5_kt_1",
)
for i, s in enumerate(signals):
    sl = get(f"{s}_2B2WToLNu2J")
    dl = get(f"{s}_2B2VTo2L2Nu")
    ls = dl.label_short.replace(
        r"HH$, $H \rightarrow b\bar{b}$, $H \rightarrow VV$ (DL)",
        r"HH \rightarrow b\bar{b}VV$" + "\n",
    ).replace("\n ", "\n")
    if s in ("qqHH_CV_1_C2V_1_kl_1", "ggHH_kl_1_kt_1"):
        ls = re.sub(r"\s*\(NLO\) -.+", " (NLO, SM)", ls)
    p = aci.Process(
        name=f"{s}_2B2V",
        id=1900 + i,
        label=dl.label.replace(" (DL)", ""),
        label_short=ls,
        processes=[sl, dl],
    )
    # add to parent
    get("diHiggs").processes.add(p)
    specific_signals.append(p)

# add all processes now
analysis.processes.extend(specific_processes + specific_signals)

# corrections which are only used for distinct analyses
analysis.aux["non_common_corrections"] = [
    "DYEstFull",
    "Fake",
    "VJetsCorrections",
    "BTagSubjetCorrections",
]

#
# process groups
#

# shorten label for plotting:
# fmt: off
# analysis.processes.get("ggHH_kl_1_kt_1_2B2V").label_short = "$gg \\rightarrow HH \\rightarrow b\\bar{b}VV$ (SM)"
# analysis.processes.get("qqHH_CV_1_C2V_1_kl_1_2B2V").label_short = "$VBF \\rightarrow HH \\rightarrow b\\bar{b}VV$ (SM)"
# shorten it like in bbtautau
analysis.processes.get("ggHH_kl_1_kt_1_2B2V").label_short = "HH (ggF)"
analysis.processes.get("qqHH_CV_1_C2V_1_kl_1_2B2V").label_short = "HH (VBF)"
# fmt: on


def pgrp(name: str, children: tuple, **kwargs) -> str:
    ret = analysis.processes.new(name=name, processes=list(map(get, children)), **kwargs)
    sig = get("diHiggs")
    if any(map(sig.has_process, ret.processes)):
        sig.processes.add(ret)
    return ret.name


plotting_procs = [
    "tt",
    "st",
    "plot_vv(v)",
    "plot_other",
    "plot_H",
    "fakes",
    # "ggHH_kl_1_kt_1_2B2Tau",
    # "qqHH_CV_1_C2V_1_kl_1_2B2VTo2L2Nu",
    # "qqHH_CV_1_C2V_1_kl_1_2B2Tau",
    "qqHH_CV_1_C2V_1_kl_1_2B2V",
    "ggHH_kl_1_kt_1_2B2V",  # "ggHH_kl_1_kt_1_2B2VTo2L2Nu",  # "GluGluToHHTo2B2VTo2L2Nu_node_SM",
    "data",
]
analysis.aux["process_groups"] = {
    # just defines the grouping during plotting/inference
    "plotting_DYMC": plotting_procs + ["dy"],
    "plotting_DYest": plotting_procs + ["DYest"],
    "plotting": plotting_procs + ["DYest"],
    "compact": [
        # fmt: off
        pgrp("compact_top", ["tt", "st"], color=rgb(193, 117, 166), label="Top"),
        pgrp("compact_dyvvv", ["DYest", "plot_vv(v)"], color=rgb(161, 193, 129), label="DY+Multiboson"),
        pgrp("compact_fake", ["fakes"], color=rgb(168, 218, 220), label="Misid. lep."),
        pgrp("compact_other", ["plot_other", "plot_H"], color=rgb(249, 216, 117), label="Other"),
        pgrp("ggHH_compact", ["ggHH_kl_1_kt_1_2B2V", "ggHH_kl_1_kt_1_2B2Tau"], color=rgb(200, 37, 6), label="HH (ggF)"),
        pgrp("qqHH_compact", ["qqHH_CV_1_C2V_1_kl_1_2B2V", "qqHH_CV_1_C2V_1_kl_1_2B2Tau"], color=rgb(6, 37, 200), label="HH (VBF)"),
        # fmt: on
        "data",
    ],
}

analysis.aux["btag_sf_shifts"] = [
    "lf",
    "lfstats1",
    "lfstats2",
    "hf",
    "hfstats1",
    "hfstats2",
    "cferr1",
    "cferr2",
]

analysis.aux["sync"] = {
    "lookup_export": {
        (b"mu", b"m"),
        (b"ee", b"ee"),
        (b"emu", b"em"),
        (b"mumu", b"mm"),
        (b"min_dhi_jet", b"min_dphi_jet"),
        (b"vbf_tag", b"VBF_tag"),
        (b"electron", b"ele"),
        (b"muon", b"mu"),
        (b"jet", b"ak4Jet"),
        (b"fat", b"ak8Jet"),
        (b"weight_gen_weight", b"MC_weight"),
        (b"weight_pileup", b"PU_weight"),
    },
    "lookup_import": {
        ("l1_E", "lep0_energy"),
        ("l1_Px", "lep0_x"),
        ("l1_Py", "lep0_y"),
        ("l1_Pz", "lep0_z"),
        ("l1_pdgId", "lep0_pdgId"),
        ("l1_charge", "lep0_charge"),
        ("l2_E", "lep1_energy"),
        ("l2_Px", "lep1_x"),
        ("l2_Py", "lep1_y"),
        ("l2_Pz", "lep1_z"),
        ("l2_pdgId", "lep1_pdgId"),
        ("l2_charge", "lep1_charge"),
        ("j1_E", "jet0_energy"),
        ("j1_Px", "jet0_x"),
        ("j1_Py", "jet0_y"),
        ("j1_Pz", "jet0_z"),
        ("j1_btag", "jet0_btagDeepFlavB"),
        ("j2_E", "jet1_energy"),
        ("j2_Px", "jet1_x"),
        ("j2_Py", "jet1_y"),
        ("j2_Pz", "jet1_z"),
        ("j2_btag", "jet1_btagDeepFlavB"),
        ("j3_E", "jet2_energy"),
        ("j3_Px", "jet2_x"),
        ("j3_Py", "jet2_y"),
        ("j3_Pz", "jet2_z"),
        ("j3_btag", "jet2_btagDeepFlavB"),
        ("j4_E", "jet3_energy"),
        ("j4_Px", "jet3_x"),
        ("j4_Py", "jet3_y"),
        ("j4_Pz", "jet3_z"),
        ("j4_btag", "jet3_btagDeepFlavB"),
        ("fatjet_E", "fat0_energy"),
        ("fatjet_Px", "fat0_x"),
        ("fatjet_Py", "fat0_y"),
        ("fatjet_Pz", "fat0_z"),
        ("fatjet_tau1", "fat0_tau1"),
        ("fatjet_tau2", "fat0_tau2"),
        ("fatjet_tau3", "fat0_tau3"),
        ("fatjet_tau4", "fat0_tau4"),
        ("fatjet_softdrop", "fat0_msoftdrop"),
        ("weight_trigger_ee_sf", "trigger_ee_sf"),
        ("weight_trigger_mumu_sf", "trigger_mumu_sf"),
        ("weight_trigger_emu_sf", "trigger_emu_sf"),
        ("lepton_IDSF_recoToLoose", "lepton_tth_tight"),
        ("lepton_IDSF_looseToTight", "lepton_tth_relaxed"),
        ("lepton_IDSF", "lepton_id_loose"),
        # ( 'weight_electron_reco_low',),
        # ( 'weight_electron_reco_high',),
        # ( 'weight_muon_idiso_loose',),
        # ( 'weight_electron_id_loose_01',),
        # ( 'weight_electron_id_loose_02',),
        # ( 'weight_electron_tth_loose',),
        # ( 'weight_muon_tth_loose',),
        ("weight_electron_id_loose", "electron_id_loose"),
        ("weight_muon_idiso_loose", "muon_idiso_loose"),
        ("weight_electron_tth_tight", "electron_tth_tight"),
        ("weight_electron_tth_relaxed", "electron_tth_relaxed"),
        ("weight_muon_tth_tight", "muon_tth_tight"),
        ("weight_muon_tth_relaxed", "muon_tth_relaxed"),
        # ( 'L1prefire',),
        ("weight_l1_ecal_prefiring", "l1_ecal_prefiring"),
        # ( 'fakeRate',),
        ("weight_fake_electrons", "fake_electrons"),
        ("weight_fake_muons", "fake_muons"),
        ("weight_fake_two_non_tight", "fake_two_non_tight"),
        # ( 'weight_fake_is_mc', ),
        # ( 'PU_jetID_SF',),
        ("weight_jet_PUid_efficiency", "jet_PUid_efficiency"),
        ("weight_jet_PUid_mistag", "jet_PUid_mistag"),
        # ( 'btag_SF', - same as btagWeight in next row),
        ("weight_ak4BtagWeight", "btagWeight"),
        # ( 'btag_ratio_SF', ),
        ("weight_ak4BtagNorm", "btagNorm"),
        ("weight_ak8BtagWeight", "btagWeight_subjet"),
        ("weight_PSWeight_ISR", "PSWeight_ISR"),
        ("weight_PSWeight_FSR", "PSWeight_FSR"),
        # ( 'weight_scaleWeight',),
        # ( 'weight_LHEScaleWeight_len',),
        # ( 'weight_LHEScaleWeight_0',),
        # ( 'weight_LHEScaleWeight_1',),
        # ( 'weight_LHEScaleWeight_2',),
        # ( 'weight_LHEScaleWeight_3',),
        # ( 'weight_LHEScaleWeight_4',),
        # ( 'weight_LHEScaleWeight_5',),
        # ( 'weight_LHEScaleWeight_6',),
        # ( 'weight_LHEScaleWeight_7',),
        # ( 'weight_LHEScaleWeight_8',),
        # ( 'weight_LHEScaleWeight_9',),
        ("MC_weight", "gen_weight"),
        ("PU_weight", "pileup"),
        ("eventWeight", "total_weight"),
        ("met_E", "met_energy"),
        ("met_Px", "met_x"),
        ("met_Py", "met_y"),
        ("met_Pz", "met_z"),
    },
    "categories": ["is_all_incl_sr"],
    "eventnr": "eventnr",
    "int_vars": ["eventnr", "dataset_id"],
}

PrepareConfig(
    analysis,
    # defines which datasets we will use
    processes=[
        "data",
        "HH_2B2VTo2L2Nu",
        "HH_2B2WToLNu2J",
        "HH_2B2Tau",
        "H",
        "tt",
        "dy_lep_10To50",  # remove for new prod
        "dy_lep_50ToInf",  # remove for new prod
        "dy_lep_nj",
        "DYJetsToLL_M_50_incl",  # remove for new prod
        "DYJetsToLL_M_50_HT",  # remove for new prod
        "vv",
        "st",
        "WJetsToLNu",
        "WnJetsToLNu",
        "WJetsToLNu_HT",
        "ttV",
        "ttVV",
        "ttVH",
        "vvv",
        "rare",
    ],
    skip_process=lambda process: process.name.endswith(HHres["resonance"]),
    ignore_datasets=[
        "TTToHadronic",
        # "THQ_ctcvcp_4f_Hincl",
    ],  # , "ZH_HToBB_ZToLL_M125", "TTWW"],
    allowed_exclusive_processes=[
        "wjets_lep",
        "dy_50to",
    ],
)

HHReweigthing.update_config(
    config=analysis,
    root_processes={
        "HH_2B2VTo2L2Nu_GluGlu": BR_HH_BBVV_DL,
        "HH_2B2WToLNu2J_GluGlu": BR_HH_BBWW_SL,
        "HH_2B2Tau_GluGlu": BR_HH_BBTAUTAU,
    },
    benchmarks=(
        [dict(kl=x) for x in HHxs["gf_nnlo_ftapprox"].keys()]
        + [
            dict(
                name=f"BM12_{i}",
                **{k: gfHHparams[k][i] for k in gfHHparams.dtype.names},
            )
            for i in range(1, len(gfHHparams))  # skip first, since SM := kl=1
        ]
        + [
            dict(
                name="BM12_8a" if i == 0 else f"BM7_{i}",
                **{k: gfHHparamsNoSample[k][i] for k in gfHHparamsNoSample.dtype.names},
            )
            for i in range(len(gfHHparamsNoSample))
        ]
    ),
)
#     base_xs=None,
# )

# analysis.aux["process_groups"]["plotting_HHReweighting"] = [
#     "ggHH_kl_0_kt_1_2B2VTo2L2Nu",
#     "HH_2B2VTo2L2Nu_GluGlu_reweight_0_1_0_0_0",
#     "ggHH_kl_1_kt_1_2B2VTo2L2Nu",
#     "HH_2B2VTo2L2Nu_GluGlu_reweight_1_1_0_0_0",
#     "ggHH_kl_2p45_kt_1_2B2VTo2L2Nu",
#     "HH_2B2VTo2L2Nu_GluGlu_reweight_2p45_1_0_0_0",
#     "ggHH_kl_5_kt_1_2B2VTo2L2Nu",
#     "HH_2B2VTo2L2Nu_GluGlu_reweight_5_1_0_0_0",
# ]
# analysis.processes.get("ggHH_kl_0_kt_1_2B2VTo2L2Nu").label_short = "kl0-kt1"
# analysis.processes.get("ggHH_kl_0_kt_1_2B2VTo2L2Nu").aux["style"] = dict(
#     linestyle="--", color=(0.650, 0.027, 0.243)
# )
# analysis.processes.get("ggHH_kl_1_kt_1_2B2VTo2L2Nu").label_short = "kl1-kt1"
# analysis.processes.get("ggHH_kl_1_kt_1_2B2VTo2L2Nu").aux["style"] = dict(
#     linestyle="--", color=(0.027, 0.121, 0.650)
# )
# analysis.processes.get("ggHH_kl_2p45_kt_1_2B2VTo2L2Nu").label_short = "kl2p45-kt1"
# analysis.processes.get("ggHH_kl_2p45_kt_1_2B2VTo2L2Nu").aux["style"] = dict(
#     linestyle="--", color=(0.086, 0.650, 0.027)
# )
# analysis.processes.get("ggHH_kl_5_kt_1_2B2VTo2L2Nu").label_short = "kl5-kt1"
# analysis.processes.get("ggHH_kl_5_kt_1_2B2VTo2L2Nu").aux["style"] = dict(
#     linestyle="--", color=(0.807, 0.807, 0.011)
# )

# analysis.processes.get("HH_2B2VTo2L2Nu_GluGlu_reweight_0_1_0_0_0").label_short = "kl0-kt1 (rew.)"
# analysis.processes.get("HH_2B2VTo2L2Nu_GluGlu_reweight_0_1_0_0_0").aux["style"] = dict(
#     linestyle=":", color=(0.650, 0.027, 0.243)
# )
# analysis.processes.get("HH_2B2VTo2L2Nu_GluGlu_reweight_1_1_0_0_0").label_short = "kl1-kt1 (rew.)"
# analysis.processes.get("HH_2B2VTo2L2Nu_GluGlu_reweight_1_1_0_0_0").aux["style"] = dict(
#     linestyle=":", color=(0.027, 0.121, 0.650)
# )
# analysis.processes.get(
#     "HH_2B2VTo2L2Nu_GluGlu_reweight_2p45_1_0_0_0"
# ).label_short = "kl2p45-kt1 (rew.)"
# analysis.processes.get("HH_2B2VTo2L2Nu_GluGlu_reweight_2p45_1_0_0_0").aux["style"] = dict(
#     linestyle=":", color=(0.086, 0.650, 0.027)
# )
# analysis.processes.get("HH_2B2VTo2L2Nu_GluGlu_reweight_5_1_0_0_0").label_short = "kl5-kt1 (rew.)"
# analysis.processes.get("HH_2B2VTo2L2Nu_GluGlu_reweight_5_1_0_0_0").aux["style"] = dict(
#     linestyle=":", color=(0.807, 0.807, 0.011)
# )

class_HHGluGlu_NLO_reweight = aci.Process(
    name="class_HHGluGlu_NLO_reweight",
    id=17789297,
    label="HH(ggF)",
    processes=[
        analysis.processes.get("HH_2B2VTo2L2Nu_GluGlu_reweight"),
        analysis.processes.get("HH_2B2Tau_GluGlu_reweight"),
    ],
)

analysis.processes.extend([class_HHGluGlu_NLO_reweight])

analysis.aux["multiclass"] = MulticlassConfig(
    group="default",
    groups={
        "default": {
            "class_HHGluGlu_NLO": {"groups": ["signal", "constrain", "fit"]},
            "class_HHVBF_NLO": {"groups": ["signal", "constrain", "fit"]},
            "H": {"groups": ["background"]},
            "dy": {"groups": ["background", "constrain"]},
            "st": {"groups": ["background"]},
            "tt": {"groups": ["background", "constrain"]},
            "class_ttVX": {"groups": ["background"]},
            "class_multiboson": {"groups": ["background"]},
            "class_rare": {"groups": ["background"]},
        },
        "reweight": {
            "class_HHGluGlu_NLO_reweight": {"groups": ["signal", "constrain", "fit"]},
            "class_HHVBF_NLO": {"groups": ["signal", "constrain", "fit"]},
            "H": {"groups": ["background"]},
            "dy": {"groups": ["background", "constrain"]},
            "st": {"groups": ["background"]},
            "tt": {"groups": ["background", "constrain"]},
            "class_ttVX": {"groups": ["background"]},
            "class_multiboson": {"groups": ["background"]},
            "class_rare": {"groups": ["background"]},
        },
    },
)

from bbww_dl.config.variables import setup_variables

setup_variables(analysis)

from bbww_dl.config.categories import setup_categories

setup_categories(analysis)

#
# other configurations
#

analysis.aux["signal_process"] = "diHiggs"

for campaign in analysis.campaigns.values:
    year = campaign.aux["year"]
    campaign.aux["files"]["histo"]["DYEstimation"] = {
        (
            dt,
            ch,
            var,
        ): f"/afs/cern.ch/user/f/fbury/public/HHWeightsDYEstimation/weights/root/weight_{var}_{ch_fn}_{dt}_1D_{year}.root"
        for ch, ch_fn in [("ee", "ElEl"), ("mumu", "MuMu"), ("ll", "SF")]
        for var in ["fatjetsoftDropmass", "HT"]
        for dt in ["data", "mc"]
    }

analysis.campaigns.get("Run2_pp_13TeV_2017").aux["files"]["sync"] = {
    "louvain": "/afs/cern.ch/user/f/fbury/public/HHSynchronization/sync_Louvain_2017.root"
}

analysis.aux["binning_categories"] = {
    "2016": [
        "*_boosted_1b_?r_dnn_node_H",
        "*_boosted_1b_?r_dnn_node_class_HHGluGlu_NLO",
        "*_boosted_1b_?r_dnn_node_class_HHVBF_NLO",
        "*_boosted_1b_?r_dnn_node_class_multiboson",
        "*_boosted_1b_?r_dnn_node_class_rare",
        "*_boosted_1b_?r_dnn_node_class_ttVX",
        "*_boosted_1b_?r_dnn_node_dy",
        "*_boosted_1b_?r_dnn_node_st",
        "*_boosted_1b_?r_dnn_node_tt",
        "*_boosted_1b_?r_grp_dy_vv",
        "*_boosted_1b_?r_grp_other_notop",
        "*_boosted_1b_?r_grp_other_top",
        "*_boosted_1b_?r_grp_top",
        "*_incl_?r_dnn_node_class_multiboson",
        "*_incl_?r_dnn_node_class_rare",
        "*_incl_?r_dnn_node_class_ttVX",
        "*_incl_?r_dnn_node_dy",
        "*_incl_?r_dnn_node_st",
        "*_incl_?r_dnn_node_tt",
        "*_incl_?r_grp_dy_vv",
        "*_incl_?r_grp_other_notop",
        "*_incl_?r_grp_other_top",
        "*_incl_?r_grp_top",
        "*_resolved_1b_?r_dnn_node_H",
        "*_resolved_1b_?r_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_1b_?r_dnn_node_class_HHVBF_NLO",
        "*_resolved_2b_?r_dnn_node_H",
        "*_resolved_2b_?r_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_2b_?r_dnn_node_class_HHVBF_NLO",
        "*_resolved_?r_dnn_node_class_multiboson",
        "*_resolved_?r_dnn_node_class_rare",
        "*_resolved_?r_dnn_node_class_ttVX",
        "*_resolved_?r_dnn_node_dy",
        "*_resolved_?r_dnn_node_st",
        "*_resolved_?r_dnn_node_tt",
        "*_resolved_?r_grp_dy_vv",
        "*_resolved_?r_grp_other_notop",
        "*_resolved_?r_grp_other_top",
        "*_resolved_?r_grp_top",
    ],
    "2017": [
        "*_boosted_1b_?r_dnn_node_H",
        "*_boosted_1b_?r_dnn_node_class_HHGluGlu_NLO",
        "*_boosted_1b_?r_dnn_node_class_HHVBF_NLO",
        "*_boosted_1b_?r_dnn_node_class_multiboson",
        "*_boosted_1b_?r_dnn_node_class_rare",
        "*_boosted_1b_?r_dnn_node_class_ttVX",
        "*_boosted_1b_?r_dnn_node_dy",
        "*_boosted_1b_?r_dnn_node_st",
        "*_boosted_1b_?r_dnn_node_tt",
        "*_boosted_1b_?r_grp_dy_vv",
        "*_boosted_1b_?r_grp_other_notop",
        "*_boosted_1b_?r_grp_other_top",
        "*_boosted_1b_?r_grp_top",
        "*_incl_?r_dnn_node_class_multiboson",
        "*_incl_?r_dnn_node_class_rare",
        "*_incl_?r_dnn_node_class_ttVX",
        "*_incl_?r_dnn_node_dy",
        "*_incl_?r_dnn_node_st",
        "*_incl_?r_dnn_node_tt",
        "*_incl_?r_grp_dy_vv",
        "*_incl_?r_grp_other_notop",
        "*_incl_?r_grp_other_top",
        "*_incl_?r_grp_top",
        "*_resolved_1b_?r_dnn_node_H",
        "*_resolved_1b_?r_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_1b_?r_dnn_node_class_HHVBF_NLO",
        "*_resolved_2b_?r_dnn_node_H",
        "*_resolved_2b_?r_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_2b_?r_dnn_node_class_HHVBF_NLO",
        "*_resolved_?r_dnn_node_class_multiboson",
        "*_resolved_?r_dnn_node_class_rare",
        "*_resolved_?r_dnn_node_class_ttVX",
        "*_resolved_?r_dnn_node_dy",
        "*_resolved_?r_dnn_node_st",
        "*_resolved_?r_dnn_node_tt",
        "*_resolved_?r_grp_dy_vv",
        "*_resolved_?r_grp_other_notop",
        "*_resolved_?r_grp_other_top",
        "*_resolved_?r_grp_top",
    ],
    "2018": [
        "*_boosted_1b_?r_dnn_node_H",
        "*_boosted_1b_?r_dnn_node_class_HHGluGlu_NLO",
        "*_boosted_1b_?r_dnn_node_class_HHVBF_NLO",
        "*_boosted_1b_?r_dnn_node_class_multiboson",
        "*_boosted_1b_?r_dnn_node_class_rare",
        "*_boosted_1b_?r_dnn_node_class_ttVX",
        "*_boosted_1b_?r_dnn_node_dy",
        "*_boosted_1b_?r_dnn_node_st",
        "*_boosted_1b_?r_dnn_node_tt",
        "*_boosted_1b_?r_grp_dy_vv",
        "*_boosted_1b_?r_grp_other_notop",
        "*_boosted_1b_?r_grp_other_top",
        "*_boosted_1b_?r_grp_top",
        "*_incl_?r_dnn_node_class_multiboson",
        "*_incl_?r_dnn_node_class_rare",
        "*_incl_?r_dnn_node_class_ttVX",
        "*_incl_?r_dnn_node_dy",
        "*_incl_?r_dnn_node_st",
        "*_incl_?r_dnn_node_tt",
        "*_incl_?r_grp_dy_vv",
        "*_incl_?r_grp_other_notop",
        "*_incl_?r_grp_other_top",
        "*_incl_?r_grp_top",
        "*_resolved_1b_?r_dnn_node_H",
        "*_resolved_1b_?r_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_1b_?r_dnn_node_class_HHVBF_NLO",
        "*_resolved_2b_?r_dnn_node_H",
        "*_resolved_2b_?r_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_2b_?r_dnn_node_class_HHVBF_NLO",
        "*_resolved_?r_dnn_node_class_multiboson",
        "*_resolved_?r_dnn_node_class_rare",
        "*_resolved_?r_dnn_node_class_ttVX",
        "*_resolved_?r_dnn_node_dy",
        "*_resolved_?r_dnn_node_st",
        "*_resolved_?r_dnn_node_tt",
        "*_resolved_?r_grp_dy_vv",
        "*_resolved_?r_grp_other_notop",
        "*_resolved_?r_grp_other_top",
        "*_resolved_?r_grp_top",
    ],
}
