# coding: utf-8
# flake8: noqa
from math import pi
import utils.aci as aci


def setup_variables(cfg):
    # met
    cfg.variables.add(
        aci.Variable(
            name="MET",
            expression="met.pt",
            binning=(40, 0.0, 200.0),
            unit="GeV",
            x_title=r"$p_{T}^{miss}$",
        )
    )

    # fmt: off
    for expr, binning, title, tags in [
        ("met_ld", (20, 0.0, 200.0), r"$p_{T,LD}^{miss}$", {"fit", "shifts", "has_category", "cat_hl"}),
        ("m_ll", (25, 0.0, 400.0), r"$m_{ll}$", {"fit", "shifts", "has_category", "cat_hl"}),
        ("m_bb", (25, 0.0, 400.0), r"$m_{bb}$", set()),
        ("m_bb_bregcorr", (25, 0.0, 1000.0), r"$m_{bb}^{reg}$", {"fit", "shifts", "has_category", "cat_hl"}),
        ("mww_simplemet", (25, 0.0, 1000.0), r"$m_{WW}^{simplemet}$", set()),
        ("m_hh_simplemet_bregcorr", (25, 0.0, 1000.0), r"$m_{hh}^{simplement,reg}$", {"fit", "shifts", "has_category", "cat_hl"}),
        ("ht", (19, 50.0, 1000.0), r"$H_T$", {"fit", "shifts", "has_category", "cat_hl"}),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=expr,
                expression=expr,
                binning=binning,
                unit="GeV",
                x_title=title,
                tags=tags,
            )
        )
    # fmt: on

    for expr, title in [
        ("min_dhi_jet", r"min $\|\Delta\phi\|$ Jets"),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=expr,
                expression=expr,
                binning=(32, 0, pi),
                x_title=title,
            )
        )

    for expr, title, tags in [
        ("dr_ll", r"$\Delta$R Lep Lep", set()),
        ("dr_bb", r"$\Delta$R bJets", {"fit", "shifts", "has_category", "cat_hl"}),
        ("min_dr_jet", r"min $\Delta$R Jets", set()),
    ]:
        cfg.variables.add(
            aci.Variable(name=expr, expression=expr, binning=(25, 0, 5), x_title=title, tags=tags)
        )

    cfg.variables.add(
        aci.Variable(
            name="HT",
            expression="ht",
            binning=(200, 0.0, 1000.0),  # needed for DYest, dont change/disable!
            unit="GeV",
            x_title=r"$H_T$",
        )
    )

    # di-objs
    for name, title in [
        ("dijet", "Jet"),
        ("dilep", "Lepton"),
        ("dibjet", "bJet"),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=f"{name}_pT",
                expression=f"util.normalize(ak.firsts({name}.pt))",
                binning=(60, 0.0, 300.0),
                unit="GeV",
                x_title=rf"Di-{title} $p_T$",
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"{name}_mass",
                expression=f"util.normalize(ak.firsts({name}.mass))",
                binning=(50, 0.0, 500.0),
                unit="GeV",
                x_title=rf"Di-{title} Mass",
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"{name}_deltaR",
                expression=f"util.normalize(ak.firsts({name}.deltaR))",
                binning=(50, 0, 5),
                x_title=rf"Di-{title} $\Delta$R",
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"{name}_deltaPhi",
                expression=f"util.normalize(np.abs(ak.firsts({name}.deltaPhi)))",
                binning=(32, 0, pi),
                x_title=rf"Di-{title} $\|\Delta\phi\|$",
            )
        )
    # other mult obj vars
    cfg.variables.add(
        aci.Variable(
            name=f"min_deltaR_jet",
            expression=f"min_dr_jet",
            binning=(50, 0, 5),
            x_title=r"min $\Delta$R Jets",
        )
    )
    cfg.variables.add(
        aci.Variable(
            name=f"min_deltaPhi_jet",
            expression=f"min_dhi_jet",
            binning=(32, 0, pi),
            x_title=r"min $\|\Delta\phi\|$ Jets",
        )
    )
    cfg.variables.add(
        aci.Variable(
            name=f"min_dr_leps_b1",
            expression=f"min_dr_leps_b1",
            binning=(32, 0, pi),
            x_title=r"min $\|\Delta\phi\|$(Leptons, bJet1)",
        )
    )
    cfg.variables.add(
        aci.Variable(
            name=f"min_dr_leps_b2",
            expression=f"min_dr_leps_b2",
            binning=(32, 0, pi),
            x_title=r"min $\|\Delta\phi\|$(Leptons, bJet2)",
        )
    )
    cfg.variables.add(
        aci.Variable(
            name=f"dphi_met_dilep",
            expression=f"dphi_met_dilep",
            binning=(32, 0, pi),
            x_title=r"$\|\Delta\phi\|$(MET, Di-Lepton)",
        )
    )
    cfg.variables.add(
        aci.Variable(
            name=f"dphi_met_dibjet",
            expression=f"dphi_met_dibjet",
            binning=(32, 0, pi),
            x_title=r"$\|\Delta\phi\|$(MET, Di-bJet)",
        )
    )
    cfg.variables.add(
        aci.Variable(
            name=f"dr_dilep_dijet",
            expression=f"dr_dilep_dijet",
            binning=(50, 0, 5),
            x_title=r"min $\Delta$R(Di-Lepton, Di-Jet)",
        )
    )
    cfg.variables.add(
        aci.Variable(
            name=f"dr_dilep_dibjet",
            expression=f"dr_dilep_dibjet",
            binning=(50, 0, 5),
            x_title=r"min $\Delta$R(Di-Lepton, Di-bJet)",
        )
    )

    cfg.variables.add(
        aci.Variable(
            name="vbf_pair_mass",
            expression="vbf_pair_mass",
            binning=(200, 0.0, 1000.0),
            unit="GeV",
            x_title=r"VBF Pair m",
        )
    )

    cfg.variables.add(
        aci.Variable(
            name=f"vbf_pairs_absdeltaeta",
            expression=f"vbf_pairs_absdeltaeta",
            binning=(25, 0, 5.0),
            x_title=r"VBF Pair $|\Delta\eta|$",
        )
    )

    # tags
    for name, tag in [
        ("VBF", "vbf"),
        ("Boosted", "boosted"),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=f"{tag}_tag",
                expression=f"{tag}_tag",
                binning=(2, 0, 2),
                x_title=rf"{name} Tag",
            )
        )

    # leptons
    for name, var in [("Lepton1", "lep1"), ("Lepton2", "lep2")]:
        cfg.variables.add(
            aci.Variable(
                name=f"min_dr_jets_{var}",
                expression=f"min_dr_jets_{var}",
                binning=(50, 0, 5),
                x_title=rf"min $\Delta$R(Jets, {name})",
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"{var}_conept",
                expression=f"{var}_conept",
                binning=(40, 0.0, 200.0),
                x_title=rf"{name} $p_T^{{cone}}$",
            )
        )

    for name, var, label in [
        ("jet", "good_jets", "Jet"),
        ("bjet", "bjets", "B-Jet"),
        ("fatjet", "good_fatjets", "fat Jet"),
        ("fatbjet", "good_fatbjets", "fat B-Jet"),
    ]:
        fat = name.startswith("fat")
        cfg.variables.add(
            aci.Variable(
                name=f"n{name}s",
                expression=f"util.normalize(ak.num({var}))",
                binning=(10, 0.0, 4.0 if fat else 10.0),
                x_title=rf"Number of {label}",
            )
        )
        for i in range(2):
            cfg.variables.add(
                aci.Variable(
                    name=f"{name}{i}_pt",
                    expression=f"util.normalize(ak.pad_none({var}.pt, {i + 1}, clip=True)[:, {i}])",
                    binning=(52, 200.0, 1500.0) if fat else (40, 0.0, 200.0),
                    unit="GeV",
                    x_title=rf"{label} {i + 1} $p_T$",
                )
            )
            cfg.variables.add(
                aci.Variable(
                    name=f"{name}{i}_eta",
                    expression=f"util.normalize(ak.pad_none({var}.eta, {i + 1}, clip=True)[:, {i}])",
                    binning=(25, -2.5, 2.5),
                    x_title=rf"{label} {i + 1} $\eta$",
                )
            )
            # cfg.variables.add(
            # aci.Variable(
            #     name=f"{name}{i}_phi",
            #     expression=f"util.normalize(ak.pad_none({var}.phi, {i + 1}, clip=True)[:, {i}])",
            #     binning=(32, -pi, pi),
            #     x_title=fr"{label} {i + 1} $\phi$",
            # ))
            if fat:
                cfg.variables.add(
                    aci.Variable(
                        name=f"{name}{i}_msoftdrop",
                        expression=f"util.normalize(ak.pad_none({var}.msoftdrop, {i + 1}, clip=True)[:, {i}])",
                        binning=(30, 0, 300),  # needed for DYest, dont change/disable!
                        x_title=rf"{label} {i + 1} $\phi$",
                    )
                )
            else:
                cfg.variables.add(
                    aci.Variable(
                        name=f"{name}{i}_btagDeepFlavB",
                        expression=f"util.normalize(ak.pad_none({var}.btagDeepFlavB, {i + 1}, clip=True)[:, {i}])",
                        binning=(25, 0, 1),
                        x_title=rf"{label} {i + 1} $Tag^B_{{DeepFlavour}}$",
                    )
                )

    # leptons
    for name, expr, title in [
        ("lep0", "lead", "leading lepton"),
        ("lep1", "sub", "subleading lepton"),
    ]:
        # electrons
        cfg.variables.add(
            aci.Variable(
                name=f"{name}_pt",
                expression=f"util.normalize(ak.firsts({expr}.pt))",
                binning=(60, 0.0, 300.0),
                unit="GeV",
                x_title=rf"{title} $p_T$",
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"{name}_eta",
                expression=f"util.normalize(ak.firsts({expr}.eta))",
                binning=(25, -2.5, 2.5),
                x_title=rf"{title} $\eta$",
            )
        )

    for name, title in [
        ("lep", "Lepton"),
        ("electron", "Electron"),
        ("muon", "Muon"),
        ("jet", "Jet"),
        ("presel_vbf_jets", "Presel VBF Jet"),
        ("fat", "AK8 Jet"),
    ]:
        for i in range(4):
            for feat, binning, unit in (
                ("energy", (60, 0, 300), "GeV"),
                ("x", (60, 0, 300), "GeV"),
                ("y", (60, 0, 300), "GeV"),
                ("z", (60, 0, 300), "GeV"),
                ("pt", (60, 0, 300), "GeV"),
                ("cone_pt", (60, 0, 300), "GeV"),
                ("eta", (25, -2.5, 2.5), "1"),
                ("phi", (32, -pi, pi), "1"),
                ("pdgId", (20, 0, 20), "1"),
                ("charge", (3, -1, 1), "1"),
                ("btagDeepFlavB", (25, 0, 1), "1"),
                ("msoftdrop", (60, 0, 300), "GeV"),
            ):
                if f"{name}{i}_{feat}" in cfg.variables.keys:
                    continue
                cfg.variables.add(
                    aci.Variable(
                        name=f"{name}{i}_{feat}",
                        binning=binning,
                        unit=unit,
                        x_title=rf"{name} {i+1} {feat}",
                        aux={"histogram": False},
                    )
                )

    for name, title in [
        ("met", "MET"),
    ]:
        for feat, binning in (
            ("energy", (60, 0, 300)),
            ("x", (60, 0, 300)),
            ("y", (60, 0, 300)),
            ("z", (60, 0, 300)),
            ("pt", (60, 0, 300)),
            ("eta", (25, -2.5, 2.5)),
            ("phi", (32, -pi, pi)),
        ):
            if f"{name}_{feat}" in cfg.variables.keys:
                continue
            cfg.variables.add(
                aci.Variable(
                    name=f"{name}_{feat}",
                    binning=binning,
                    unit="GeV",
                    x_title=rf"{name} {feat}",
                    aux={"histogram": False},
                )
            )

    # single histogram with max dnn prediction output
    cfg.variables.add(
        aci.Variable(
            name="dnn_score_max",
            expression="np.max(pred, axis=-1)",
            binning=(400, 0.0, 1.0),
            unit="",
            x_title=r"DNN score",
            tags={"fit", "shifts", "dnn_score"},
        )
    )

    # weights
    cfg.variables.add(
        aci.Variable(
            name="total_weight",
            binning=(100, 0.0, 0.2),  # optimal for GGF sync tuple
            unit="1",
            x_title=r"$w_{tot}$",
            aux={"histogram": False},
        )
    )
    cfg.variables.add(
        aci.Variable(
            name="gen_weight",
            binning=(100, 0.0, 0.2),  # optimal for GGF sync tuple
            unit="1",
            x_title=r"$w_{gen}$",
            aux={"histogram": False},
        )
    )
    for weight in (
        "PSWeight_ISR",
        "PSWeight_FSR",
        "trigger_ee_sf",
        "trigger_emu_sf",
        "trigger_mumu_sf",
        "btagNorm",
    ):
        cfg.variables.add(
            aci.Variable(
                name=weight,
                binning=(100, 0.8, 1.2),  # optimal for GGF sync tuple
                unit="1",
                x_title=weight,
                aux={"histogram": False},
            )
        )

    for cf in ("cutflow", "cutflow_raw"):
        cfg.variables.add(
            aci.Variable(
                name=cf,
                aux={"histogram": False},
            )
        )
