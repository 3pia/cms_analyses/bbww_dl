# coding: utf-8

"""
Physics constants.
"""


import scinum as sn

# constants
N_LEPS = sn.Number(3)
Z_MASS = sn.Number(91.1876, {"z_mass": 0.0021})
BR_H_BB = sn.Number(0.5824, {"br_hbb": ("rel", 0.0124, 0.0127)})
BR_H_WW = sn.Number(0.2137, {"br_hww": ("rel", 0.0154, 0.0153)})
BR_W_HAD = sn.Number(0.6741, {"br_whad": 0.0027})
BR_W_LEP = 1 - BR_W_HAD
BR_WW_SL = 2 * BR_W_HAD.mul(BR_W_LEP, rho=-1, inplace=False)
BR_WW_DL = BR_W_LEP ** 2
BR_WW_FH = BR_W_HAD ** 2
