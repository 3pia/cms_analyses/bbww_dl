import os
import json
import numpy as np
from collections.abc import MutableMapping


class DYlnN(MutableMapping):
    def __init__(self, data, name_map={}):
        self._data = data
        self._name_map = name_map
        if isinstance(self.data, str):
            with open(self.data) as json_file:
                data = json.load(json_file)
                self.__dict__.update(data)
        elif isinstance(self.data, dict):
            self.__dict__.update(self.data)
        else:
            raise ValueError(f"data type {type(self.data)} can not be interpreted!")

    def __setitem__(self, key, value):
        raise Exception(
            f"All values are already stored in {self.data}. calling '__setitem__' is prohibited!"
        )

    @property
    def data(self):
        return self._data

    @property
    def name_map(self):
        return self._name_map

    def __getitem__(self, key):
        if self.name_map:
            assert key in self.name_map
            key = self.name_map.get(key)
        v = self.__dict__[key]
        assert v > 0.0
        if v > 1.0:
            u = v - 1.0
        else:
            u = 1.0 - v
        return (np.clip(1.0 - u, a_min=0.01, a_max=None), 1.0 + u)

    def __delitem__(self, key):
        raise Exception(
            f"All values are already stored in {self.data}. calling '__delitem__' is prohibited!"
        )

    def __contains__(self, key):
        return key in [k[0] for k in self._name_map.keys()]

    def __iter__(self):
        return iter(self.__dict__)

    def __len__(self):
        return len(self.__dict__)


# Category key mapping: internal namings <-> Louvain namings
# fmt: off
name_map = {
    ("all_resolved_1b_sr_dnn_node_class_HHGluGlu_NLO", 2016): "HH_resolved1b_GGF_2016",
    ("all_resolved_1b_sr_dnn_node_H", 2016): "HH_resolved1b_H_2016",
    ("all_resolved_1b_sr_dnn_node_class_HHVBF_NLO", 2016): "HH_resolved1b_VBF_2016",
    ("all_resolved_2b_sr_dnn_node_class_HHGluGlu_NLO", 2016): "HH_resolved2b_GGF_2016",
    ("all_resolved_2b_sr_dnn_node_H", 2016): "HH_resolved2b_H_2016",
    ("all_resolved_2b_sr_dnn_node_class_HHVBF_NLO", 2016): "HH_resolved2b_VBF_2016",
    ("all_resolved_1b_sr_dnn_node_class_HHGluGlu_NLO", 2017): "HH_resolved1b_GGF_2017",
    ("all_resolved_1b_sr_dnn_node_H", 2017): "HH_resolved1b_H_2017",
    ("all_resolved_1b_sr_dnn_node_class_HHVBF_NLO", 2017): "HH_resolved1b_VBF_2017",
    ("all_resolved_2b_sr_dnn_node_class_HHGluGlu_NLO", 2017): "HH_resolved2b_GGF_2017",
    ("all_resolved_2b_sr_dnn_node_H", 2017): "HH_resolved2b_H_2017",
    ("all_resolved_2b_sr_dnn_node_class_HHVBF_NLO", 2017): "HH_resolved2b_VBF_2017",
    ("all_resolved_1b_sr_dnn_node_class_HHGluGlu_NLO", 2018): "HH_resolved1b_GGF_2018",
    ("all_resolved_1b_sr_dnn_node_H", 2018): "HH_resolved1b_H_2018",
    ("all_resolved_1b_sr_dnn_node_class_HHVBF_NLO", 2018): "HH_resolved1b_VBF_2018",
    ("all_resolved_2b_sr_dnn_node_class_HHGluGlu_NLO", 2018): "HH_resolved2b_GGF_2018",
    ("all_resolved_2b_sr_dnn_node_H", 2018): "HH_resolved2b_H_2018",
    ("all_resolved_2b_sr_dnn_node_class_HHVBF_NLO", 2018): "HH_resolved2b_VBF_2018",
    ("all_boosted_1b_sr_dnn_node_class_HHGluGlu_NLO", None): "HH_boosted_GGF",
    ("all_boosted_1b_sr_dnn_node_H", None): "HH_boosted_H",
    ("all_boosted_1b_sr_dnn_node_class_HHVBF_NLO", None): "HH_boosted_VBF",
}
# fmt: on

dy_lnN_Louvain = DYlnN(os.path.abspath("bbww_dl/models/factorsDY_alleras.json"), name_map=name_map)

# update 'dy_lnN_RWTH' with dy ncc factors calculated by the GroupProcesses hook:
dy_lnN_RWTH = DYlnN(
    {
        "all_boosted_1b_sr_dnn_node_class_HHGluGlu_NLO_2016": 0.18165764421255207,
        "all_boosted_1b_sr_dnn_node_class_HHVBF_NLO_2016": 0.2050518311439537,
        "all_resolved_1b_sr_dnn_node_class_HHGluGlu_NLO_2016": 0.7671515759883827,
        "all_resolved_1b_sr_dnn_node_class_HHVBF_NLO_2016": 0.43814486730615815,
        "all_resolved_2b_sr_dnn_node_class_HHGluGlu_NLO_2016": 1.1638345100862706,
        "all_resolved_2b_sr_dnn_node_class_HHVBF_NLO_2016": 0.750916895336823,
        "all_boosted_1b_sr_dnn_node_class_HHGluGlu_NLO_2017": 0.23010720408328786,
        "all_boosted_1b_sr_dnn_node_class_HHVBF_NLO_2017": 0.4099256366475036,
        "all_resolved_1b_sr_dnn_node_class_HHGluGlu_NLO_2017": 0.8905557273573336,
        "all_resolved_1b_sr_dnn_node_class_HHVBF_NLO_2017": 0.7828077866676909,
        "all_resolved_2b_sr_dnn_node_class_HHGluGlu_NLO_2017": 0.5445453350078275,
        "all_resolved_2b_sr_dnn_node_class_HHVBF_NLO_2017": 1.480486929683448,
        "all_boosted_1b_sr_dnn_node_class_HHGluGlu_NLO_2018": 0.14836283574992926,
        "all_boosted_1b_sr_dnn_node_class_HHVBF_NLO_2018": 0.41711013852697837,
        "all_resolved_1b_sr_dnn_node_class_HHGluGlu_NLO_2018": 0.9425020795675318,
        "all_resolved_1b_sr_dnn_node_class_HHVBF_NLO_2018": 0.8201535983433321,
        "all_resolved_2b_sr_dnn_node_class_HHGluGlu_NLO_2018": 0.23927180783591734,
        "all_resolved_2b_sr_dnn_node_class_HHVBF_NLO_2018": 0.8183422030524309,
    }
)
