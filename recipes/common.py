# -*- coding: utf-8 -*-

from warnings import warn

import numpy as np
import awkward as ak

from bbww_dl.config.analysis import analysis

Pget = analysis.processes.get

from utils.tf_serve import autoClient
from processor.util import reduce_and, reduce_or, make_p4
import tasks.corrections.processors as corr_proc

from collections import defaultdict


def ll(leptons, *aux, use_conept=False):
    leptons_tpls = ak.combinations(
        leptons[:, :2],
        n=2,
        replacement=False,
        axis=-1,
        fields=["l1", "l2"],
    )
    pair = leptons_tpls.l1 + leptons_tpls.l2
    if use_conept:
        pt1 = ak.any(leptons_tpls.l1.cone_pt >= 25.0, axis=-1)
        pt2 = ak.any(leptons_tpls.l2.cone_pt >= 15.0, axis=-1)
    else:
        pt1 = ak.any(leptons_tpls.l1.pt >= 25.0, axis=-1)
        pt2 = ak.any(leptons_tpls.l2.pt >= 15.0, axis=-1)
    oc = ak.any(pair.charge == 0, axis=-1)
    # m = (xc.mass <= 76.0).any().any()
    return (leptons_tpls.l1, leptons_tpls.l2, pair, reduce_and(pt1, pt2, oc, *aux))


def gen_match(lep, flavs=[1, 15]):
    return reduce_or(*(lep.genPartFlav == f for f in flavs))


class Base:
    common = ("energy", "x", "y", "z")
    debug_dataset = "GluGluToHHTo2B2VTo2L2Nu_node_cHHH2p45"  # "TTTo2L2Nu"  # "WWW_4F"  # "data_B_ee" # 2018: "data_B_e+ee"
    # debug_dataset = "TTTT" # has lots of jets
    # debug_dataset = "TTGJets" # has bad Jet_getJetIdx
    # debug_dataset = "ZZZ"  # has 5F partons but reweights to 4F PDF-Set, how? 70AE2D7F-A32B-A243-ACBB-D64F61564CB2
    # debug_dataset = "GluGluToHHTo2B2VTo2L2Nu_node_cHHH1", "GluGluToHHTo2B2VTo2L2Nu_node_cHHH2p45"
    # debug_dataset = "DYJetsToLL_0J"
    # debug_dataset = "WJetsToLNu"
    # debug_dataset = "ST_t-channel_top_4f_inclusiveDecays" # bad PDFSet info
    # debug_uuids = {
    #     "14CDC9DE-13D7-2A45-8B0B-10A0B4B2576C",
    #     "338794ED-8B95-5C4D-A46A-652886DEFFC9",
    #     "CC513CCC-C8DB-E84C-9B73-7F1C36111C4D",
    # }
    fat_jet_vars = ("tau1", "tau2", "tau3", "tau4", "msoftdrop")
    hl = (
        "m_bb_bregcorr",
        "ht",
        "min_dr_jets_lep1",
        "min_dr_jets_lep2",
        "m_ll",
        "dr_ll",
        "min_dr_jet",
        "min_dhi_jet",
        "m_hh_simplemet_bregcorr",
        "met_ld",
        "dr_bb",
        "min_dr_leps_b1",
        "min_dr_leps_b2",
        "lep1_conept",
        "lep2_conept",
        "mww_simplemet",
        "vbf_tag",
        "boosted_tag",
        "dphi_met_dilep",
        "dphi_met_dibjet",
        "dr_dilep_dijet",
        "dr_dilep_dibjet",
        "vbf_pair_mass",
        "vbf_pairs_absdeltaeta",
    )
    param = ("year",)

    def obj_arrays(self, X, n, extra=()):
        assert 0 < n and n == int(n)
        cols = self.common + extra

        def pp(x):
            return x if n is True else ak.to_numpy(ak.fill_none(ak.pad_none(x, n, clip=True), 0))

        return np.stack(
            [pp(getattr(X, a)).astype(np.float32) for a in cols],
            axis=-1,
        )

    @property
    def tensors(self):
        common = self.common
        fat_jet_vars = self.fat_jet_vars
        hl = self.hl
        param = self.param
        # fmt: off
        return {
            "lep": ("good_leptons", 2, common + ("pdgId", "charge"), np.float32, {"groups": ["multiclass", "eval", "input", "part"]}),
            "jet": ("good_jets", 4, common + ("btagDeepFlavB",), np.float32, {"groups": ["multiclass", "eval", "input", "part"], "blackout": [np.s_[:, -1:]]}),
            "fat": ("good_fatbjets", 1, common + fat_jet_vars, np.float32, {"groups": ["multiclass", "eval", "input", "part"]}),
            "met": ("metp4", 0, common, np.float32, {"groups": ["multiclass", "eval", "input", "part"]}),
            "hl": (None, 0, hl, np.float32, {"groups": ["multiclass", "eval", "input"]}),
            "param": (None, 0, param, np.float32, {"groups": ["multiclass", "eval", "input"]}),
            "eventnr": (None, 0, ["eventnr"], np.int64, {"groups": ["multiclass", "eval", "split"]}),
            "procid": (None, 0, ["procid"], np.int32, {"groups": ["multiclass", "class"]}),
        }
        # fmt: on

    def arrays(self, X):
        out = {}
        for name, tensor in self.tensors.items():
            id, n, vars, typ, aux = tensor

            if id is None or id in X.keys():
                pass
            else:
                continue

            def preproc(x):
                return ak.to_numpy(
                    x if n == 0 else ak.fill_none(ak.pad_none(x, n, clip=True), 0),
                    allow_missing=False,
                )

            if id is None:
                vals = [preproc(X[var]) for var in vars]
                vals = np.stack(vals, axis=-1)
            elif id == "weights":
                vals = []
                for var in vars:
                    attr = "_weights"
                    if var.endswith(("Up", "Down")):
                        attr = "_modifiers"
                    weight = preproc(getattr(X[id], attr)[var])
                    vals.append(weight)
                vals = np.stack(vals, axis=-1)
            elif id == "pred":
                vals = X[id]
            else:
                vals = [preproc(getattr(X[id], var)) for var in vars]
                vals = np.stack(vals, axis=-1)
            # set all nans or infs to 0
            out[name] = np.nan_to_num(vals, nan=0.0, posinf=0.0, neginf=-0.0).astype(typ)
        return out

    def datasets_arrays(self, select_output, dataset):
        if dataset is None:
            return {}
        tensor = self.tensors.get("procid", ())
        assert tensor[:3] == (None, 0, ["procid"])
        (process,) = self.campaign_inst.datasets.get(dataset).processes.values
        return {"procid": select_output["const_arr"](process.id)[:, None].astype(tensor[3])}

    def get_pu_key(self, events):
        ds = self.get_dataset(events)
        if ds.is_data:
            return "data"
        else:
            return ds.name


class MCOnly:
    @classmethod
    def requires(cls, task):
        return task.base_requires(data_source="mc")


class Exporter:
    sep = "+"
    group = "default"
    skip_cats = {"0b", "fr"}

    @classmethod
    def live_callback(cls, accumulator):
        ret = defaultdict(int)
        for cat, num in super().live_callback(accumulator).items():
            ret[cat.split(cls.sep, 1)[0]] += num
        return ret

    def select(self, events):
        out = next(super().select(events))

        out["categories"] = {
            key: val
            for key, val in out["categories"].items()
            if not (set(key.split("_")) & self.skip_cats)
        }

        # create inclusive training category
        incl = out["selection"].add(
            "incl",
            reduce_or(*(out["selection"].all(*v) for v in out["categories"].values())),
        )
        out["categories"] = {"all_inlusive_sr": ["incl"]}

        datasets = out.setdefault("datasets", {})
        datasets.setdefault(None, 1)
        for dsn, dsfactor in datasets.items():
            dataset = (
                self.get_dataset(events) if dsn is None else self.campaign_inst.datasets.get(dsn)
            )
            (process,) = dataset.processes.values

            w = process.xsecs[self.campaign_inst.ecm].nominal
            w *= self.campaign_inst.aux["lumi"]
            if w > 0:
                datasets[dsn] = dsfactor * w
            else:
                warn(f"xsec*lumi weight of {w} for datasets {dataset} is not positive")

        yield out

    @classmethod
    def save(cls, *args, **kwargs):
        kwargs["sep"] = cls.sep
        return super().save(*args, **kwargs)


class DNNBase:
    build_dnn_cats = False

    def __init__(self, task):
        super().__init__(task)

        self.dnn = autoClient(self.dnn_model, remote=False)

    @property
    def dnn_model(self):
        raise NotImplementedError

    @property
    def multiclass_processes(self):
        multiclass_config = self.analysis_inst.aux["multiclass"]
        groups = multiclass_config.groups
        group = multiclass_config.group
        return groups[group]

    def category_variables(self, category):
        if "_dnn_node_" in category:
            return [v for v in self.variables if v.name.startswith("dnn_score_max")]
        else:
            return self.variables

    def variable_full_shifts(self, variable):
        return "shifts" in variable.tags

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            selection = ret["selection"]
            categories = ret["categories"]
            # DNN score:
            mask = reduce_or(*(selection.all(*cuts) for cuts in categories.values()))
            arr = self.arrays(ret)
            dnn_inputs = {}
            for k, v in arr.items():
                if k in self.tensors:
                    groups = self.tensors[k][-1].get("groups", [])
                    if "multiclass" in groups and "eval" in groups:
                        dnn_inputs[k] = v[mask]
            (pm,) = self.dnn(inputs=dnn_inputs).values()
            target_shape = (mask.sum(), len(self.multiclass_processes))
            assert pm.shape == target_shape, (pm.shape, target_shape)
            assert not np.any(np.isnan(pm)), "DNN produces nan values"

            pred = np.full(mask.shape[:1] + pm.shape[1:], np.nan, dtype=pm.dtype)
            pred[mask] = pm
            pmax = np.argmax(pred, axis=-1)
            catkeys = [c for c in categories.keys() if not (set(c.split("_")) & self.skip_cats)]
            if self.build_dnn_cats:
                for i, c in enumerate(self.multiclass_processes.keys()):
                    m = pmax == i
                    cn = f"dnn_node_{c}"
                    selection.add(cn, m)
                    for k in catkeys:
                        cuts = categories[k]
                        cuts_dnn = cuts + [cn]
                        categories[f"{k}_{cn}"] = cuts_dnn
            ret["pred"] = pred
            yield ret


class PUCount(Base, corr_proc.PUCount):
    memory = "1000MiB"


class HHCount(Base, corr_proc.HHCount):
    memory = "2000MiB"
    # this particular files will produce Mhh < 250 if wrong gen H are selected
    debug_dataset = "GluGluToHHTo2B2VTo2L2Nu_node_9"
    debug_uuids = ["70813227-FFDD-6148-AC5B-7CFD515D207B"]


class VJetsStitchCount(Base, corr_proc.VJetsStitchCount):
    memory = "1000MiB"


class VptSFCount(Base, corr_proc.VptSFCount):
    debug_dataset = "DYJetsToLL_M-10to50"
    memory = "1000MiB"
