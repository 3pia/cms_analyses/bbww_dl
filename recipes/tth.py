# coding: utf-8

import warnings
import awkward as ak
import numpy as np
import json
import re
from functools import reduce, partial
from operator import mul
from coffea.nanoevents.methods import vector
from coffea.processor.accumulator import defaultdict_accumulator, dict_accumulator
import hist
import matplotlib.pyplot as plt
from scinum import DOWN, UP, Number
from scipy.optimize import curve_fit

from processor.generator import GeneratorHelper
import processor.util as util
import processor.bbww as bbww
from processor.fake import ff, non_closure_correction
from tasks.corrections.btag import BTagSF_reshape, BTagSubjetCorrections
import tasks.corrections.processors as corr_proc
from tasks.corrections.fake import HistBinLookup
from utils.coffea import (
    ArrayExporter,
    Histogramer,
    PackedSelection,
    TreeExporter,
    Weights,
    BaseProcessor,
)
from utils.plot import label, auto_ratio_ylim
from . import common
from .common import (
    ll,
    gen_match,
    MCOnly,
    # need to be explicitly present in module:
    PUCount,
    HHCount,
    VJetsStitchCount,
    VptSFCount,
)
from utils.util import view_sdtype_flat, pscan_vispa
from utils.bh5 import Histogram as Hist5
from utils.topvariations import TopSystematics, topvariations
from tqdm.auto import tqdm


class Base(common.Base):
    dnn_folds = 5

    @property
    def trigger(self):
        return {
            2016: {
                "ee": {
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                "e": {
                    "Ele27_WPTight_Gsf": all,
                    "Ele25_eta2p1_WPTight_Gsf": all,
                    "Ele27_eta2p1_WPLoose_Gsf": all,  # Heavily prescaled in run H
                },
                "emu": {
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL": all,  # Missing in run H
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,  # Introduced mid run F
                    "Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL": all,  # Missing in run H
                    "Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL_DZ": "H",  # Only in run H
                },
                "mumu": {
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL": all,  # Prescaled in run H by average factor of 26.5
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ": all,
                    "Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL": all,  # Prescaled in run H by average factor of 26.5
                    "Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_DZ": all,
                },
                "mu": {
                    "IsoMu22": all,  # Prescaled since mid run E til the end of 2016
                    "IsoTkMu22": all,  # Prescaled since mid run E til the end of 2016
                    "IsoMu22_eta2p1": all,  # Introduced mid run B
                    "IsoTkMu22_eta2p1": all,  # Introduced mid run B
                    "IsoMu24": all,
                    "IsoTkMu24": all,
                },
            },
            2017: {
                "ee": {
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL": all,
                },
                "e": {
                    "Ele35_WPTight_Gsf": all,
                    "Ele32_WPTight_Gsf": all,  # Missing in runs B and C
                },
                "emu": {
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL": all,  # Missing in run B
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                "mumu": {
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8": all,
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8": all,  # Missing in run B
                },
                "mu": {
                    "IsoMu24": all,  # Prescaled at high lumi
                    "IsoMu27": all,
                },
            },
            2018: {
                "ee": {
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL": all,
                },
                "e": {
                    "Ele32_WPTight_Gsf": all,
                },
                "emu": {
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL": all,
                },
                "mumu": {
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8": all,
                },
                "mu": {
                    "IsoMu24": all,
                    "IsoMu27": all,
                },
            },
        }[int(self.year)]

    def select(self, events):
        output = self.accumulator.identity()

        # get meta infos
        dataset_key = tuple(events.metadata["dataset"])
        dataset_inst = self.get_dataset(events)

        # inject variables
        events["Muon", "cone_pt"] = bbww.conept_mu(events.Muon)
        events["Electron", "cone_pt"] = bbww.conept_ele(events.Electron)
        events["Muon", "tight_cand"] = events.Muon.mediumId & (events.Muon.mvaTTH >= 0.5)
        events["Electron", "tight_cand"] = events.Electron.mvaTTH >= 0.3
        if dataset_inst.is_mc:
            events["Muon", "gen_matched"] = gen_match(events.Muon)
            events["Electron", "gen_matched"] = gen_match(events.Electron)

        # BTagSF injection
        #  Subjet
        BTagSubjetCorrections.prepare(events, self.corrections, wp="medium")

        # filter NaNs arising from broken events with broken MET
        # e.g.: debug_dataset, debug_uuids = "WJetsToLNu_HT-100To200", {"DB4EE882-D7FB-984A-A1E0-D3E3DADCD19D"}, 1st chunk
        # Event No: 54867
        goodMET = np.isfinite(events.MET.pt)
        if not np.all(goodMET):
            warnings.warn(f"{np.sum(~goodMET)}/{len(events)} MET events are NaN, dropping them")
            events = events[goodMET]

        # filter bad PDF
        if dataset_inst.is_mc:
            events = self.corrections["pdf"].filterBad(events)

        n_events = len(events)
        eventnr = ak.to_numpy(events.event)

        def const_arr(value):
            arr = np.broadcast_to(value, shape=(n_events,))
            arr.flags.writeable = False
            return arr

        # uuid - file finder debug variable
        uuid = self.get_lfn(events, default="").rsplit("/", 1)[-1].split(".", 1)[0].replace("-", "")
        uuid = [const_arr(int(uuid[i], 16) if i < len(uuid) else 17) for i in range(32)]

        dataset_id = dataset_inst.id * np.ones(n_events)

        weight_bad = const_arr(np.float32(np.nan))

        # early category definition
        common = [
            "lumimask",
            "met_filter",
            "lumimask",
            "ge_two_fakeable_leptons",
            "leq_two_tight_leps",
            "gen_matched",
            "low_mass_resonances",
            "zee_mass_window_cut",
            "zmumu_mass_window_cut",
        ]
        dim1 = {"ee": ["ch_ee"], "mumu": ["ch_mumu"], "emu": ["ch_emu"]}
        dim2 = {
            "resolved_0b_to1b": ["!>=1fatbjet", ">=2jets", "==0bjet", "!>=1fatjet", "DYest_even"],
            "resolved_0b_to2b": ["!>=1fatbjet", ">=2jets", "==0bjet", "!>=1fatjet", "!DYest_even"],
            "resolved_1b": ["!>=1fatbjet", ">=2jets", "==1bjet"],
            "resolved_2b": ["!>=1fatbjet", ">=2jets", ">=2bjet"],
            "boosted_0b": [">=1fatjet", "!>=1fatbjet", "==0bjet"],
            "boosted_1b": [">=1fatjet", ">=1fatbjet"],
        }
        dim3 = {"sr": ["sr"], "fr": ["fr"]}

        # load objects
        muons = events.Muon
        electrons = events.Electron

        # adjust sorting
        muons = muons[ak.argsort(muons.cone_pt, ascending=False)]
        electrons = electrons[ak.argsort(electrons.cone_pt, ascending=False)]

        # dataset shifts
        datasets = {None: 1}  # default: no additional datasets
        if dataset_inst.is_mc:
            if "hhreweigthing" in self.corrections:
                datasets.update(
                    self.corrections["hhreweigthing"].generate(
                        events, dataset_inst, self.analysis_inst
                    )
                )

        for unc, shift, met, jets, fatjets in self.jec_loop(events):
            # adjust sorting
            jets = jets[ak.argsort(jets.pt, ascending=False)]
            fatjets = fatjets[ak.argsort(fatjets.pt, ascending=False)]

            weights = Weights(n_events, dtype=np.float32, storeIndividual=self.individual_weights)
            # correct phi MET modulation
            met = self.corrections["metphi"](met, events.PV.npvs, dataset_inst)

            # start object cutflow
            output["object_cutflow"]["total muons"] += ak.sum(ak.num(muons))
            output["object_cutflow"]["total electrons"] += ak.sum(ak.num(electrons))
            output["object_cutflow"]["total jets"] += ak.sum(ak.num(jets))

            # muons
            presel_muons = bbww.presel_mu(muons)
            output["object_cutflow"]["preselected muons"] += ak.sum(ak.num(presel_muons))
            fakeable_muons = bbww.fakeable_mu(presel_muons, year=self.year)
            output["object_cutflow"]["fakeable muons"] += ak.sum(ak.num(fakeable_muons))

            # electrons
            presel_electrons_preclean = bbww.presel_ele(electrons)
            # preselected electrons need to be cleaned wrt preselected muons
            presel_electrons = presel_electrons_preclean[
                util.nano_object_overlap(presel_electrons_preclean, presel_muons, dr=0.3)
            ]
            output["object_cutflow"]["preselected electrons"] += ak.sum(ak.num(presel_electrons))
            fakeable_electrons = bbww.fakeable_ele(presel_electrons, year=self.year)
            output["object_cutflow"]["fakeable electrons"] += ak.sum(ak.num(fakeable_electrons))

            # build jet masks
            jID = "isLoose" if self.year == "2016" else "isTight"
            mask_jets_central = util.reduce_and(
                getattr(jets, jID),
                jets.pt >= 25.0,
                np.abs(jets.eta) <= 2.4,
            )
            mask_jets_vbf = util.reduce_and(
                getattr(jets, jID),
                jets.pt >= 30.0,
                np.abs(jets.eta) <= 4.7,
                util.reduce_or(
                    jets.pt >= 60,
                    np.abs(jets.eta) < 2.7,
                    np.abs(jets.eta) > 3.0,
                ),
            )
            mask_jets_puid_loose = util.reduce_or(
                ak.values_astype(((jets.puId >> 2) & 1), bool),
                (jets.pt > 50),
            )

            # apply mask & puID
            presel_jets = jets[mask_jets_central & mask_jets_puid_loose]
            presel_vbf_jets = jets[mask_jets_vbf & mask_jets_puid_loose]
            output["object_cutflow"]["presel jets"] += ak.sum(ak.num(presel_jets))

            # fat jets
            good_fatjets = fatjets[
                util.reduce_and(
                    getattr(fatjets, jID),
                    fatjets.pt >= 200.0,
                    np.abs(fatjets.eta) <= 2.4,
                    fatjets.msoftdrop > 30,
                    fatjets.msoftdrop < 210,
                    (fatjets.tau2 / fatjets.tau1) <= 0.75,
                    ak.num(fatjets.subjets, axis=-1) == 2,
                    ak.all(fatjets.subjets.pt >= 20, axis=-1),
                    ak.all(np.abs(fatjets.subjets.eta) <= 2.4, axis=-1),
                )
            ]

            fakeable_leptons = ak.with_name(
                ak.concatenate([fakeable_muons, fakeable_electrons], axis=1), "PtEtaPhiMCandidate"
            )
            fakeable_leptons = fakeable_leptons[
                ak.argsort(fakeable_leptons.cone_pt, ascending=False)
            ]
            two = fakeable_leptons[:, :2]

            # now clean all objects
            def cleanagainst_leptons(toclean, dr):
                return toclean[
                    util.nano_object_overlap(
                        toclean=toclean, cleanagainst=fakeable_leptons[:, :2], dr=dr
                    )
                ]

            presel_jets = cleanagainst_leptons(presel_jets, 0.4)
            output["object_cutflow"]["clean jets"] += ak.sum(ak.num(presel_jets))

            presel_vbf_jets = cleanagainst_leptons(presel_vbf_jets, 0.4)
            good_fatjets = cleanagainst_leptons(good_fatjets, 0.8)

            output["n_events"][dataset_key] = n_events

            # met filter
            if dataset_inst.is_data:
                met_filter = self.campaign_inst.aux["met_filters"]["data"]
            else:
                met_filter = self.campaign_inst.aux["met_filters"]["mc"]

            # selection
            selection = PackedSelection()
            selection.add("leq_two_tight_leps", ak.sum(fakeable_leptons.tight_cand, axis=-1) <= 2)
            selection.add("sr", ak.sum(fakeable_leptons[:, :2].tight_cand, axis=-1) == 2)
            selection.add("fr", ak.sum(fakeable_leptons[:, :2].tight_cand, axis=-1) < 2)

            met_filter_mask = util.nano_mask_and(events.Flag, met_filter)
            selection.add("met_filter", met_filter_mask)

            if dataset_inst.is_data:
                selection.add("lumimask", self.corrections["lumimask"](events))
            else:
                selection.add("lumimask", np.ones(n_events, dtype=bool))

            # select two leptons
            selection.add("ge_two_fakeable_leptons", ak.num(fakeable_leptons) >= 2)

            # select 2 jets
            selection.add(">=2jets", ak.num(presel_jets) >= 2)

            # select at least one bjet
            n_btag = ak.sum(presel_jets.btagDeepFlavB >= bbww.btag_wp[self.year]["medium"], axis=-1)
            selection.add("==0bjet", n_btag == 0)
            selection.add("==1bjet", n_btag == 1)
            selection.add(">=2bjet", n_btag >= 2)

            # remove J/Psi and co
            presel_leptons = ak.with_name(
                ak.concatenate([presel_muons, presel_electrons_preclean], axis=1),
                "PtEtaPhiMCandidate",
            )
            presel_leptons_tpls = ak.combinations(
                presel_leptons,
                n=2,
                replacement=False,
                axis=-1,
                fields=["l1", "l2"],
            )
            selection.add(
                "low_mass_resonances",
                ak.all((presel_leptons_tpls.l1 + presel_leptons_tpls.l2).mass > 12, axis=-1),
            )

            # mll cut for ee and mumu
            muon_tpls = ak.combinations(
                presel_muons,
                n=2,
                replacement=False,
                axis=-1,
                fields=["m1", "m2"],
            )
            dimuon = muon_tpls.m1 + muon_tpls.m2
            zmumu_mass_window_cut = util.reduce_or(
                np.abs(dimuon.mass - 91.1876) > 10.0,
                dimuon.charge != 0,
            )
            selection.add("zmumu_mass_window_cut", ak.all(zmumu_mass_window_cut, axis=-1))

            electron_tpls = ak.combinations(
                presel_electrons,
                n=2,
                replacement=False,
                axis=-1,
                fields=["e1", "e2"],
            )
            dielectron = electron_tpls.e1 + electron_tpls.e2
            zee_mass_window_cut = util.reduce_or(
                abs(dielectron.mass - 91.1876) > 10.0,
                dielectron.charge != 0,
            )
            selection.add("zee_mass_window_cut", ak.all(zee_mass_window_cut, axis=-1))

            # boosted selection/tag
            selection.add(
                ">=1fatjet", ak.any(ak.any(good_fatjets.subjets.pt >= 30, axis=-1), axis=-1)
            )

            fat_bjets_mask = ak.any(
                util.reduce_and(
                    good_fatjets.subjets.btagDeepB
                    >= self.campaign_inst.aux["working_points"]["subjet_deepb"]["medium"],
                    good_fatjets.subjets.pt >= 30,
                ),
                axis=-1,
            )
            ge_one_fatbjet = ak.sum(fat_bjets_mask, axis=-1) > 0
            selection.add(">=1fatbjet", ge_one_fatbjet)

            # include untagged jets in DYestimation source region
            good_fatbjets = good_fatjets[fat_bjets_mask | selection.all(*dim2["boosted_0b"])]
            boosted_tag = util.reduce_or(
                *(selection.all(*v) for k, v in dim2.items() if k.startswith("boosted_"))
            )

            # vbf cleaning
            presel_vbf_jets = presel_vbf_jets[
                util.reduce_or(
                    util.nano_object_overlap(
                        toclean=presel_vbf_jets,
                        cleanagainst=presel_jets[
                            ak.argsort(presel_jets.btagDeepFlavB, ascending=False)
                        ][:, :2],
                        dr=0.8,
                    )
                    & ~boosted_tag,
                    util.nano_object_overlap(
                        toclean=presel_vbf_jets,
                        cleanagainst=good_fatbjets[:, :1],
                        dr=1.2,
                    )
                    & boosted_tag,
                )
            ]

            # vbf selection/tag
            vbf_pairs = ak.combinations(
                presel_vbf_jets,
                n=2,
                replacement=False,
                axis=1,
                fields=["j1", "j2"],
            )
            vbf_dijets = vbf_pairs.j1 + vbf_pairs.j2
            vbf_pairs_absdeltaeta = np.abs(vbf_pairs.j1.eta - vbf_pairs.j2.eta)
            vbf_mask = util.reduce_and(
                vbf_dijets.mass > 500,
                vbf_pairs_absdeltaeta > 3.0,
            )
            vbf_dijets = vbf_dijets[vbf_mask]
            vbf_pairs_absdeltaeta = vbf_pairs_absdeltaeta[vbf_mask]
            sort = ak.argsort(vbf_dijets.mass, ascending=False)
            vbf_dijets = vbf_dijets[sort]
            vbf_pairs_absdeltaeta = vbf_pairs_absdeltaeta[sort]

            vbf_tag = np.asarray(ak.num(vbf_dijets) > 0, int)
            selection.add("vbf_tag", ak.num(vbf_dijets) > 0)
            selection.add("ggf_tag", ak.num(vbf_dijets) == 0)

            # trigger
            trigger = util.Trigger(events.HLT, self.trigger, dataset_inst)

            # standard
            lead, sub, fake_pair, ll_mask = ll(fakeable_leptons[:, :2], use_conept=True)
            ch_mumu = (
                ll_mask
                & trigger.get("mumu", "mu")
                & (
                    ak.any(np.abs(lead.pdgId) == 13, axis=-1)
                    & ak.any(np.abs(sub.pdgId) == 13, axis=-1)
                )
            )
            ch_emu = (
                ll_mask
                & trigger.get("emu", "mu", "e")
                & (
                    (
                        ak.any(np.abs(lead.pdgId) == 11, axis=-1)
                        & ak.any(np.abs(sub.pdgId) == 13, axis=-1)
                    )
                    | (
                        ak.any(np.abs(lead.pdgId) == 13, axis=-1)
                        & ak.any(np.abs(sub.pdgId) == 11, axis=-1)
                    )
                )
            )
            ch_ee = (
                ll_mask
                & trigger.get("ee", "e")
                & (
                    ak.any(np.abs(lead.pdgId) == 11, axis=-1)
                    & ak.any(np.abs(sub.pdgId) == 11, axis=-1)
                )
            )

            selection.add("ch_mumu", ch_mumu)
            selection.add("ch_emu", ch_emu)
            selection.add("ch_ee", ch_ee)

            gen_matched = (
                ak.any(lead.gen_matched, axis=-1) & ak.any(sub.gen_matched, axis=-1)
                if dataset_inst.is_mc
                else np.ones(n_events, dtype=bool)
            )

            selection.add("gen_matched", gen_matched)

            # build categories
            categories = {
                "_".join([dim1_key, dim2_key, dim3_key]): common
                + dim1_value
                + dim2_value
                + dim3_value
                for dim1_key, dim1_value in dim1.items()
                for dim2_key, dim2_value in dim2.items()
                for dim3_key, dim3_value in dim3.items()
            }

            # weights
            if dataset_inst.is_data:
                output["sum_gen_weights"][dataset_key] = 0.0
                output["count_bad_gen_weights"][dataset_key] = 0.0
            if dataset_inst.is_mc:
                self.corrections["pdf"](events, weights, clip=10, relative=True)

                gh = GeneratorHelper(events, weights)
                gh.PSWeight(lfn=self.get_lfn(events))
                gh.ScaleWeight()

                # NOTE: all prior (nominal + their variations) weights are included in sum_gen_weight!
                gh.gen_weight(output, dataset_key, datasets)

                weights.add(
                    "pileup",
                    **self.corrections["pileup"](
                        pu_key=self.get_pu_key(events), nTrueInt=events.Pileup.nTrueInt
                    ),
                )

                # L1 ECAL prefiring
                if self.year in ("2016", "2017"):
                    weights.add(
                        "l1_ecal_prefiring",
                        events.L1PreFiringWeight.Nom,
                        weightUp=events.L1PreFiringWeight.Up,
                        weightDown=events.L1PreFiringWeight.Dn,
                    )

                if dataset_inst.name.startswith("TTTo"):
                    w_toppt = ak.to_numpy(util.top_pT_reweighting(events.GenPart)).squeeze()
                    weights.add(
                        "top_pT_reweighting",
                        w_toppt,
                        weightUp=w_toppt**2,  # w**2
                        weightDown=ak.ones_like(w_toppt),  # w = 1.
                    )

                # lepton sf
                e_corr = self.corrections["electron"]
                mu_corr = self.corrections["muon"]

                # electrons
                # fmt: off
                ## id and loose tth id
                mask = (abs(two.pdgId) == 11) & two.gen_matched
                loose_1 = e_corr[f"electron_Tallinn_id_loose_01_EGamma_SF2D"](abs(two.eta), two.pt)[mask] # using pT
                loose_1_err = e_corr[f"electron_Tallinn_id_loose_01_EGamma_SF2D_error"](abs(two.eta), two.pt)[mask] # using pT
                loose_2 = e_corr[f"electron_Tallinn_id_loose_02_EGamma_SF2D"](abs(two.eta), two.pt)[mask] # using pT
                loose_2_err = e_corr[f"electron_Tallinn_id_loose_02_EGamma_SF2D_error"](abs(two.eta), two.pt)[mask] # using pT

                if self.year in ("2016", "2017"):
                    # for 2016 and 2017 the reco eff is split into a low and high pt region
                    reco_low = e_corr[f"electron_Tallinn_reco_low_EGamma_SF2D"](two.eta, two.pt)[mask & (two.pt < 20)]  # using pT
                    reco_low_err = e_corr[f"electron_Tallinn_reco_low_EGamma_SF2D_error"](two.eta, two.pt)[mask & (two.pt < 20)] # using pT
                    reco_high = e_corr[f"electron_Tallinn_reco_high_EGamma_SF2D"](two.eta, two.pt)[mask & (two.pt >= 20)] # using pT
                    reco_high_err = e_corr[f"electron_Tallinn_reco_high_EGamma_SF2D_error"](two.eta, two.pt)[mask & (two.pt >= 20)] # using pT
                    electron_id_nom, electron_id_down, electron_id_up = (
                        reduce(
                            mul,
                            [
                                ak.prod(sf, axis=-1) for sf in [
                                    reco_low + sign * reco_low_err,
                                    reco_high + sign * reco_high_err,
                                    loose_1 + sign * loose_1_err,
                                    loose_2 + sign * loose_2_err,
                                ]
                            ]
                        )
                        for sign in (0, -1, +1)
                    )
                elif self.year == "2018":
                    reco = e_corr[f"electron_Tallinn_reco_EGamma_SF2D"](two.eta, two.pt)[mask] # using pT
                    reco_err = e_corr[f"electron_Tallinn_reco_EGamma_SF2D_error"](two.eta, two.pt)[mask] # using pT
                    electron_id_nom, electron_id_down, electron_id_up = (
                        reduce(
                            mul,
                            [
                                ak.prod(sf, axis=-1) for sf in [
                                    reco + sign * reco_err,
                                    loose_1 + sign * loose_1_err,
                                    loose_2 + sign * loose_2_err,
                                ]
                            ]
                        )
                        for sign in (0, -1, +1)
                    )
                # fmt: on
                weights.add(
                    "electron_id_loose",
                    electron_id_nom,
                    weightUp=electron_id_up,
                    weightDown=electron_id_down,
                    shift=False,
                )

                # fmt: off
                ## tight tth id - only uncertainty
                mask = (abs(two.pdgId) == 11) & two.gen_matched & two.tight_cand
                up = ak.prod(e_corr["electron_Tallinn_tth_tight_error_histo_eff_data_max"](abs(two.eta), two.pt)[mask], axis=-1)  # using pT
                down = ak.prod(e_corr["electron_Tallinn_tth_tight_error_histo_eff_data_min"](abs(two.eta), two.pt)[mask], axis=-1)  # using pT
                weights.add(
                    "electron_tth_tight",
                    np.ones(n_events),
                    weightUp=up,
                    weightDown=down,
                    shift=False,
                )

                ## relaxed tth id
                prefix = f"electron_Tallinn_tth_relaxed_EGamma_SF2D"
                weights.add(
                    "electron_tth_relaxed",
                    ak.prod(e_corr[f"{prefix}"](abs(two.eta), two.pt)[mask], axis=-1),  # using pT
                    weightUp=ak.prod(e_corr[f"{prefix}_Up"](abs(two.eta), two.pt)[mask], axis=-1),  # using pT
                    weightDown=ak.prod(e_corr[f"{prefix}_Down"](abs(two.eta), two.pt)[mask],axis=-1), # using pT
                )
                # fmt: on

                # muons
                ## id&iso
                mask = (abs(two.pdgId) == 13) & two.gen_matched
                prefix = "muon_Tallinn_idiso_loose_EGamma_SF2D"
                weights.add(
                    "muon_idiso_loose",
                    ak.prod(mu_corr[f"{prefix}"](abs(two.eta), two.pt)[mask], axis=-1),  # using pT
                    weightUp=ak.where(
                        ak.any(mask, axis=-1),
                        ak.prod(
                            mu_corr[f"{prefix}_error"](abs(two.eta), two.pt)[mask], axis=-1
                        ),  # using pT
                        0,
                    ),
                    weightDown=None,
                    shift=True,
                )

                # fmt: off
                ## tight tth id - only uncertainty
                mask = (abs(two.pdgId) == 13) & two.gen_matched & two.tight_cand
                up = ak.prod(mu_corr["muon_Tallinn_tth_tight_error_histo_eff_data_max"](abs(two.eta), two.pt)[mask], axis=-1)  # using pT
                down = ak.prod(mu_corr["muon_Tallinn_tth_tight_error_histo_eff_data_min"](abs(two.eta), two.pt)[mask], axis=-1)  # using pT
                weights.add(
                    "muon_tth_tight",
                    np.ones(n_events),
                    weightUp=up,
                    weightDown=down,
                    shift=False,
                )

                ## relaxed tth id
                prefix = f"muon_Tallinn_tth_relaxed_EGamma_SF2D"
                weights.add(
                    f"muon_tth_relaxed",
                    ak.prod(mu_corr[f"{prefix}"](abs(two.eta), two.pt)[mask], axis=-1),  # using pT
                    weightUp=ak.prod(mu_corr[f"{prefix}_Up"](abs(two.eta), two.pt)[mask], axis=-1),  # using pT
                    weightDown=ak.prod(mu_corr[f"{prefix}_Down"](abs(two.eta), two.pt)[mask], axis=-1),  # using pT
                )
                # fmt: on

                # trigger sf
                sub_pt = util.normalize(fakeable_leptons.cone_pt, pad=2, clip=True)[:, 1]
                for name, ch in [
                    ("ee", ch_ee),
                    ("emu", ch_emu),
                    ("mumu", ch_mumu),
                ]:
                    # https://gitlab.cern.ch/ttH_leptons/doc/blob/master/Legacy/data_to_mc_corrections.md#trigger-efficiency-scale-factors
                    # Always subleading cone_pt, except in 2018 for mumu channel here use leading
                    if (self.year == "2018") and (name == "mumu"):
                        sub_pt = util.normalize(fakeable_leptons.cone_pt, pad=True)
                    corr_trig = self.corrections["trigger"]["raw"]
                    weights.add(
                        f"trigger_{name}_sf",
                        ak.where(ch, corr_trig[(int(self.year), name, 0)](sub_pt), 1),
                        weightUp=ak.where(ch, corr_trig[(int(self.year), name, +1)](sub_pt), 1),
                        weightDown=ak.where(ch, corr_trig[(int(self.year), name, -1)](sub_pt), 1),
                    )

                # https://twiki.cern.ch/twiki/bin/viewauth/CMS/BTagSFMethods
                # AK8 subjet Method 1a
                BTagSubjetCorrections.apply(self, good_fatjets.subjets, weights, wp="medium")
                # AK4 Jet reshape SF
                BTagSF_reshape(self, events, weights, presel_jets, shift, unc)

                if "vjets" in self.corrections:
                    self.corrections["vjets"](dataset_inst, events, weights)

                self.corrections["jetpuid"]["loose"](
                    cleanagainst_leptons(jets[mask_jets_central | mask_jets_vbf], dr=0.4), weights
                )

            # fake background estimation
            fake_corr = self.corrections["fake"]
            for name, pdgid, identifier in [
                ("fake_electrons", 11, "FR_mva030_el_data_comb"),
                ("fake_muons", 13, "FR_mva050_mu_data_comb"),
            ]:
                prefix = f"Tallinn_fakerates_dl_{identifier}"
                mask = (abs(two.pdgId) == pdgid) & ~two.tight_cand
                nc_corr = non_closure_correction[name][int(self.year)]

                fake_rate = fake_corr[f"{prefix}"](two.cone_pt, abs(two.eta))
                fake_rate_err = fake_corr[f"{prefix}_error"](two.cone_pt, abs(two.eta))

                nominal = ak.prod(ff(nc_corr * fake_rate)[mask], axis=-1)
                weights.add(name, nominal)

                def rel_shift(fake_factor):
                    return ak.where(
                        ak.any(mask, axis=-1),
                        fake_factor / nominal,
                        1.0,
                    )

                fake_lookup = HistBinLookup(fake_corr[prefix])
                for unc_name, unc_mask_func in fake_lookup.bin_masks:
                    unc_name = f"{name}:{unc_name}"
                    unc_mask = mask * unc_mask_func(two.cone_pt, abs(two.eta))

                    up_shifted = fake_rate + fake_rate_err * unc_mask
                    down_shifted = fake_rate - fake_rate_err * unc_mask

                    weights.add(
                        unc_name,
                        ak.ones_like(nominal),
                        weightUp=rel_shift(ak.prod(ff(nc_corr * up_shifted)[mask], axis=-1)),
                        weightDown=rel_shift(ak.prod(ff(nc_corr * down_shifted)[mask], axis=-1)),
                    )

                # shift of non-closure correction
                weights.add(
                    f"{name}_non_closure",
                    ak.ones_like(nominal),
                    weightUp=rel_shift(ak.prod(ff(nc_corr**2 * fake_rate)[mask], axis=-1)),
                    weightDown=rel_shift(ak.prod(ff(fake_rate)[mask], axis=-1)),
                )

            ht = util.get_ht(presel_jets)

            # if both fail tight, multiply with -1
            weights.add(
                "fake_two_non_tight",
                ak.where(
                    ak.sum(~two.tight_cand, axis=-1) == 2,
                    np.array(-1, np.float32),
                    np.array(1, np.float32),
                ),
            )

            # DY estimation
            def DYest(name, variable, value, cat):
                """
                We have transfer factors for ll (inclusive: ee+mumu) available in `self.corrections['dyestimation']`.
                Those are applied to the inclusive category (ll) as discussed with Florian.
                """
                corr = self.corrections.get("dyestimation", None)
                if corr is None:
                    return
                cuts = dim2[cat]
                # get values
                b = f"ll_{cat}_{variable}"
                nom = corr[b](value)
                err = corr[f"{b}_error"](value)
                # apply mask
                mask = selection.all(*cuts)
                nom = ak.where(mask, nom, 1)
                err = ak.where(mask, err, 0)
                # add weight
                weights.add(
                    name=f"ll_{name}",
                    weight=nom,
                    weightUp=nom + err,
                    weightDown=nom - err,
                )

            selection.add("DYest_even", (eventnr // self.dnn_folds) % 2 == 0)
            DYest(
                name="DYest_fat",
                variable="fatjet1_msoftdrop",
                value=util.normalize(good_fatjets.msoftdrop, pad=True, fill=0),
                cat="boosted_0b",
            )
            for n in 1, 2:
                DYest(
                    name=f"DYest_jet{n}b",
                    variable="HT",
                    value=ht,
                    cat=f"resolved_0b_to{n}b",
                )

            bjets = presel_jets[ak.argsort(presel_jets.btagDeepFlavB, ascending=False)][:, :2]

            sync_electrons = two[np.abs(two.pdgId) == 11]
            sync_muons = two[np.abs(two.pdgId) == 13]

            good_jets = presel_jets
            two_cone = ak.zip(
                dict(
                    pt=two.cone_pt,
                    eta=two.eta,
                    phi=two.phi,
                    mass=two.mass,
                    pdgId=two.pdgId,
                    charge=two.charge,
                ),
                with_name="PtEtaPhiMCandidate",
            )
            good_leptons = two_cone

            metp4 = util.get_metp4(met)
            dijet = util.lead_diobj(presel_jets)
            dibjet = util.lead_diobj(bjets)
            dilep = util.lead_diobj(two_cone)

            ##################

            vbf_pair_mass = util.normalize(vbf_dijets.mass, pad=True, fill=0)
            vbf_pairs_absdeltaeta = util.normalize(vbf_pairs_absdeltaeta, pad=True, fill=0)

            dphi_met_dilep = util.normalize(np.abs(metp4.delta_phi(ak.firsts(dilep))))
            dphi_met_dibjet = util.normalize(np.abs(metp4.delta_phi(ak.firsts(dibjet))))
            dr_dilep_dijet = util.normalize(ak.firsts(dilep.delta_r(ak.firsts(dijet))))
            dr_dilep_dibjet = util.normalize(ak.firsts(dilep.delta_r(ak.firsts(dibjet))))

            m_bb = util.normalize(bjets.sum().mass)

            m_bb_bregcorr = util.normalize(util.bregcorr(bjets).sum().mass)

            min_dr_jets_lep1, min_dr_jets_lep2 = util.min_dr_part1_part2(
                two_cone, presel_jets, getn=2
            )
            min_dr_jets_lep1, min_dr_jets_lep2 = util.normalize(min_dr_jets_lep1), util.normalize(
                min_dr_jets_lep2
            )
            m_ll = util.normalize(two_cone.sum().mass)

            dr_ll = util.normalize(util.min_dr(two_cone))
            dphi_ll = util.normalize(util.min_dphi(two_cone))

            min_dr_jet = util.normalize(util.min_dr(presel_jets))
            min_dhi_jet = util.normalize(util.min_dphi(presel_jets))

            m_hh_simplemet = util.normalize((bjets.sum() + two_cone.sum() + metp4).mass)
            m_hh_simplemet_bregcorr = util.normalize(
                (util.bregcorr(bjets).sum() + two_cone.sum() + metp4).mass
            )

            met_ld = util.normalize(util.get_met_ld(presel_jets, fakeable_leptons, metp4))

            dr_bb = util.normalize(util.min_dr(bjets))
            dphi_bb = util.normalize(util.min_dphi(bjets))

            min_dr_leps_b1, min_dr_leps_b2 = util.min_dr_part1_part2(bjets, two_cone, getn=2)
            min_dr_leps_b1, min_dr_leps_b2 = util.normalize(min_dr_leps_b1), util.normalize(min_dr_leps_b2)  # fmt: skip
            lep_conept = util.normalize(two_cone.pt, pad=2, clip=True)
            lep1_conept, lep2_conept = lep_conept[:, 0], lep_conept[:, 1]
            mww_simplemet = util.normalize((two_cone.sum() + metp4).mass)

            year = const_arr(int(self.year))
            if dataset_inst.is_mc:
                (process,) = dataset_inst.processes.values
                procid = const_arr(process.id)
            else:
                procid = const_arr(self.analysis_inst.processes.get("data").id)

            yield locals()


class DNNBase(common.DNNBase):
    """
    DNN Model
    - Upload: https://gitlab.cern.ch/cms-hh-bbww/ml-models/-/tree/master/models/multi-classification/dnn/DL/13
    """

    dnn_model = "/net/scratch/cms/dihiggs/store/bbww_dl/MulticlassStitchedExport/prod41/export_version_0/training_version_paper_0/lbn_particles_12/ResNet/jump_2/layers_3/nodes_256/activation_relu/batch_norm_1/dropout_0.0/l2_1e-08/lossdef_default/optimizer_adam/learning_rate_1.00e-03/default_9/years_2016_2017_2018/model_default/cw_0p125_0p125_1_1_1_1_1_1_1/model"


class Processor(DNNBase, Base, Histogramer):
    jes_shifts = True
    dataset_shifts = True
    memory = "3000MiB" if jes_shifts else "2500MiB"
    skip_cats = set()

    debug_dataset = "TTTo2L2Nu"  # "GluGluToHHTo2B2VTo2L2Nu_node_cHHH1"
    # multiple datasets (data-driven)
    # debug_dataset, debug_uuids = "VBFHHTo2B2VTo2L2Nu_CV_1_C2V_1_C3_2", {"85417A27-F771-0C40-A89D-A439BABC2EC3"}  # fmt: skip

    @classmethod
    def group_processes(cls, hists, task):
        from rich import print

        def fakes(cat):
            if "_fr" in cat:
                return cat.replace("_fr", "_sr")

        def fakes_skip_mc(c):
            if task.analysis_inst.processes.get(c).has_parent_process(
                task.analysis_inst.processes.get("diHiggs")
            ):
                return False
            return True

        def DYest(cat):
            if "boosted_0b" in cat and "_0b_to1" not in cat:
                cat = cat.replace("boosted_0b", "boosted_0b_to1b")
            if "_0b_to" in cat:
                return cat.replace("_0b_to", "_")

        def DYest_skip_mc(c, ncc=False):
            if task.analysis_inst.processes.get(c).has_parent_process(
                task.analysis_inst.processes.get("diHiggs")
            ):
                return False
            if c in {"fakes", "DYest", "DYestMC", "dy"}:
                return False
            if ncc:
                if c == "data":
                    return False
            return True

        def data_syst(syst):
            return (
                syst.startswith("fake_")
                or "_DYest_" in syst
                or any(syst.startswith(top_systematic) for top_systematic in TopSystematics)
            )

        groups = {
            "all": "^(ee|emu|mumu)",
            "resolved": "resolved_[12]b",
            "incl": "boosted_1b|resolved_[12]b",
            "grp_dy_vv": "dnn_node_(dy|class_multiboson)$",
            "grp_other_top": "dnn_node_(st|tt|H|class_(rare|ttVX))$",
            "grp_other_notop": "dnn_node_(H|class_(rare|ttVX))$",
            "grp_top": "dnn_node_(st|tt)$",
        }

        print("perform fakes extraction:", "fakes" in task.legacy_processes)
        print("perform DYest extraction:", "DYest" in task.legacy_processes)
        for k, h in (
            tq := tqdm(hists.items(), unit="variable", desc="extract data driven modeling")
        ):
            assert h.axes.name[1] == "category"
            variable = task.analysis_inst.variables.get(k)

            # TT bar dataset shifts, only exist for variables which were processed with the full shifts.
            # tt process must be existent in the histogram
            if "shifts" in variable.tags and "tt" in h.axes["process"]:
                h += topvariations(h)

            # Fakes estimation
            if "fakes" in task.legacy_processes:
                h = cls.data_mc_substract(
                    task.analysis_inst,
                    h,
                    fakes,
                    data_syst=data_syst,
                    skip_mc=fakes_skip_mc,
                )

            # DY estimation
            if "DYest" in task.legacy_processes:
                h = cls.data_mc_substract(
                    task.analysis_inst,
                    h,
                    DYest,
                    data_syst=data_syst,
                    skip_mc=partial(DYest_skip_mc, ncc=False),
                    overwrite=False,
                    eject=False,  # need to keep 0b cats for ncc
                )

                if "shifts" in variable.tags:
                    # DY non-closure correction
                    #
                    # 1. Create 'fake data' := sum MC without DYest, fakes, signal
                    # 2. Do data-driven for 'fake data'
                    # [2.5 Group categories]
                    # 3. Select DY from data-driven for 'fake data', and from MC only
                    # 4. Rebin histograms
                    # 5. Fit linear function to ratio of histograms
                    # 6. apply
                    #

                    # 1.
                    AllMC = set(h.axes["process"]) - {"data", "DYest", "DYestMC", "fakes"}
                    AllMC = tuple(p for p in AllMC if not p.startswith(("qqHH_", "ggHH_")))
                    hmc = Hist5.regrow(h, {"process": ["AllMC"]})
                    hv = hmc.view(True).view(np.ndarray)
                    idx = h.axes["process"].index(tuple(AllMC))
                    hv[0, ...] = np.sum(h.view(True)[idx, ...], axis=0)
                    h += hmc

                    assert (
                        rps := (
                            set(AllMC)
                            - set(
                                cls.filter_mc_proc(
                                    task.analysis_inst,
                                    h,
                                    proc_data="AllMC",
                                    skip_mc=partial(DYest_skip_mc, ncc=True),
                                )
                            )
                        )
                    ) == {
                        "dy"
                    }, f"The DY ncc should substract sum MC from sum MC without DY MC only! Differences: {rps}"

                    # 2. DEBUG
                    h = cls.data_mc_substract(
                        task.analysis_inst,
                        h,
                        DYest,
                        data_syst=data_syst,
                        proc_name="DYestMC",
                        proc_data="AllMC",
                        skip_mc=partial(DYest_skip_mc, ncc=True),
                        overwrite=False,
                    )

            # remove (remaining) data-driven regions
            cats = [c for c in h.axes["category"] if "_0b_" not in c and "_fr" not in c]

            # add regrouped categories
            regroups = []
            for dst, pat in groups.items():
                cats_new = set()
                for cat in cats:
                    rem = re.sub(pat, dst, cat)
                    if rem != cat:
                        assert rem not in cats, f"new category {rem} already existing"
                        cats_new.add(rem)
                regroups.append((dst, pat, cats.copy()))
                cats.extend(sorted(cats_new))

            # regrow histograms once
            h = Hist5.regrow(h, dict(category=cats))
            hv = h.view(True).view(np.ndarray)

            for dst, pat, cats in tqdm(regroups, desc="regroup", leave=False):
                for cat in tqdm(cats, unit="category", leave=False):
                    rem = re.sub(pat, dst, cat)
                    if rem != cat:
                        a = view_sdtype_flat(hv[:, h.axes["category"].index(rem)])
                        a[...] += view_sdtype_flat(hv[:, h.axes["category"].index(cat)])

            # rest of DY ncc
            if "DYest" in task.legacy_processes and variable.tags.issuperset(
                {"shifts", "dnn_score"}
            ):
                # book systematics for ncc
                h += Hist5.regrow(
                    h,
                    dict(
                        systematic=[
                            "DYncc1Up",
                            "DYncc2Up",
                            "DYncc3Up",
                            "DYncc1Down",
                            "DYncc2Down",
                            "DYncc3Down",
                        ]
                    ),
                )
                axes = h.axes[:3]
                assert axes.name == ("process", "category", "systematic")

                hv = h.view()

                # 3.
                nomIdx = h.axes["systematic"].index("nominal")
                ddDY = hv[h.axes["process"].index("DYest"), :, nomIdx, :]
                fakeDY = hv[h.axes["process"].index("DYestMC"), :, nomIdx, :]
                mcDY = hv[h.axes["process"].index("dy"), :, nomIdx, :]

                dyncc = {}

                # 4. (only for fit categories)
                # The function is considered to approximate any methodical difference through a taylor expansion until a certain order.
                # use first order polynom for all regions as DY is expected to 'peak' at 0 and diminish towards 1
                # use second order polynom for DY+VV(V) region as DY is expected to 'peak' at 1 and also at 0.5 (max confusion with multiboson), while it diminishes towards 0
                for c in task.analysis_inst.categories.query(name=".+", tags={"fit"}):
                    cat = c.name
                    cidx = h.axes["category"].index(cat)
                    _ddDY = ddDY[cidx, ...]
                    _fakeDY = fakeDY[cidx, ...]
                    _mcDY = mcDY[cidx, ...]
                    assert _fakeDY.ndim == _mcDY.ndim == _ddDY.ndim == 1
                    assert _fakeDY.shape == _mcDY.shape == _ddDY.shape
                    if "boosted" in cat:
                        new_binning = np.r_[0:1:6j]
                    else:
                        new_binning = np.r_[0:1:21j]
                    hnew = hist.Hist(
                        hist.axis.StrCategory(["DYest", "DYestMC", "dy"], name="process"),
                        hist.axis.Variable(new_binning, name=k),
                        storage=hist.storage.Weight(),
                    )
                    loc = h.axes[k].index
                    ax = hnew.axes[k]
                    for bin in range(ax.size):
                        hnew["DYest", bin] = _ddDY[loc(ax[bin][0]):loc(ax[bin][1])].sum(axis=-1)  # fmt: skip
                        hnew["DYestMC", bin] = _fakeDY[loc(ax[bin][0]):loc(ax[bin][1])].sum(axis=-1)  # fmt: skip
                        hnew["dy", bin] = _mcDY[loc(ax[bin][0]):loc(ax[bin][1])].sum(axis=-1)  # fmt: skip

                    # 5.
                    # 1st order
                    fun = lambda x, a, b: a + b * x

                    DYestMC = Number(
                        np.clip(hnew["DYestMC", ...].view()["value"], a_min=0, a_max=np.inf),
                        np.sqrt(hnew["DYestMC", ...].view()["variance"]),
                    )
                    DYMC = Number(
                        np.clip(hnew["dy", ...].view()["value"], a_min=0, a_max=np.inf),
                        np.sqrt(hnew["dy", ...].view()["variance"]),
                    )
                    if "signal" in c.tags:
                        dyncc[f"{cat}_{task.year}"] = np.sum(hnew["dy", ...].view()["value"]) / np.sum(hnew["DYestMC", ...].view()["value"])  # fmt: skip
                    else:
                        # do ratio and fit
                        # ratio
                        with np.errstate(divide="ignore", invalid="ignore"):
                            r = DYMC / DYestMC

                        assert (
                            np.sum(good := np.isfinite(r.nominal)) > 1
                        ), f"Less non finite values than free parameters in fit function: {r.nominal}"

                        # remove values without uncertainty - unphysical. happens only for 0 +-0, which breaks the fit
                        good &= r.u(direction=UP) > 0

                        popt, pcov = curve_fit(
                            fun,
                            ax.centers[good],
                            r.nominal[good],
                            sigma=r.u(direction=UP)[good],
                            absolute_sigma=True,
                        )

                        task.output().parent.touch()
                        debug_path = task.output().parent.path + f"/debug_dyncc_{cat}.pdf"

                        with np.errstate(divide="ignore", invalid="ignore"):
                            from cycler import cycler

                            plt.rc("axes", prop_cycle=cycler(color=["#f8961e", "#43aa8b"]))

                            hnew["dy", ...].plot_ratio(
                                hnew["DYestMC", ...],
                                rp_ylabel=r"Ratio",
                                rp_denom_label=task.analysis_inst.processes.get("DYestMC").label,
                                rp_num_label="DY (from MC)",
                                rp_uncert_draw_type="bar",
                                rp_ylim=(0, 2),
                            )
                        fig = plt.gcf()

                        nax, rax = fig.axes

                        ddDY_artist = hnew["DYest", ...].plot1d(ax=nax, color="#577590")

                        label(ax=nax, data=False, campaign=task.campaign_inst)

                        ratio = fun(ax.centers, *popt)
                        ratio_up = fun(ax.centers, *(popt + np.sqrt(np.diag(pcov))))
                        ratio_do = fun(ax.centers, *(popt - np.sqrt(np.diag(pcov))))

                        rline = rax.plot(
                            ax.centers, ratio, "-", color="#a4161a", label="Linear fit"
                        )
                        rax.fill_between(
                            ax.centers, ratio_up, ratio_do, facecolor="#ba181b", alpha=0.2
                        )
                        # fix ratio ylim
                        auto_ratio_ylim(rax, ratio_max_delta=1.2, auto_log_thresh=np.inf)
                        _handles, _labels = nax.get_legend_handles_labels()
                        # ddDY
                        _handles = ddDY_artist + _handles
                        _labels = [task.analysis_inst.processes.get("DYest").label] + _labels
                        _handles += rline

                        fname = (
                            r"$f(x)=a + b\cdot x + c\cdot x^2$"
                            if "dy" in cat
                            else r"$f(x)=a + b\cdot x$"
                        )
                        _labels += [
                            fname
                            + "\n"
                            + " - $a$: "
                            + rf"${popt[0]:.2f} \pm {np.sqrt(np.diag(pcov))[0]:.2f}$"
                            + "\n"
                            + " - $b$: "
                            + rf"${popt[1]:.2f} \pm {np.sqrt(np.diag(pcov))[1]:.2f}$"
                        ]

                        nax.legend_ = None
                        nax.legend(_handles, _labels)

                        fig.suptitle(c.label)
                        plt.xlabel(task.analysis_inst.variables.get(k).x_title)

                        plt.savefig(debug_path)
                        fig.clf()

                        # 6. apply only for background
                        # eigvals, eigvecs
                        assert np.all(
                            np.isfinite(pcov)
                        ), f"{cat} - covariance matrix is not fully finite, can't do eigenvalue decomposition: {pcov}"
                        eigvals, eigvec = np.linalg.eigh(pcov)

                        rval = np.sqrt(eigvals)

                        # save nominal
                        x = h.axes[-1].centers
                        y = hv[axes.index("DYest", cat, "nominal")].copy()

                        # apply SF (also for all current systematics)

                        hv[axes[:2].index("DYest", cat)] *= fun(x, *popt)

                        # explicitly set our systematics using nominal
                        for i in range(pcov.shape[0]):
                            for shift, sign in {"Up": +1, "Down": -1}.items():
                                syst = f"DYncc{i+1}{shift}"
                                delta = sign * rval[i] * eigvec[:, i]
                                coeffs = popt + delta
                                hv[axes.index("DYest", cat, syst)] = y * fun(x, *coeffs)

                # store DY ncc lnN
                with open(
                    (dynccjson := (task.output().parent.path + f"/dyncc_{cat}.json")), "w"
                ) as f:
                    json.dump(dyncc, f, indent=4)
                print(
                    "\n[b]DY non-closure plots:[/b]\n"
                    + pscan_vispa(
                        directory=task.output().parent.path,
                        regex="debug_dyncc_(.+).pdf",
                    )
                    + "\n"
                )
                print(
                    f"\n[b]DY non-closure integral ratios ({dynccjson} - update values in 'bbww_dl/models/dy_lnn.py'):[/b]\n"
                    + json.dumps(dyncc, indent=4)
                    + "\n"
                )
            # add extra categories for variables which need standalone full syst. postfit plots
            if "has_category" in variable.tags:
                h = cls.regroup(h, groups={f"all_incl_sr_{variable.name}": "all_incl_sr"})

            hists[k] = h

        return hists


class Exporter(common.Exporter, Base, MCOnly, ArrayExporter):
    pass


class BTagSFNormEff(Base, corr_proc.BTagSFNormEff):
    skip_cuts = {"==0bjet", "==1bjet", ">=2bjet"}
    subjets = "good_fatjets.subjets"
    debug_dataset = "TTTo2L2Nu"

    @classmethod
    def requires(cls, task):
        return task.base_requires(
            data_source="mc", btagnorm=False, btagsubjet=False, dyestimation=False
        )

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            cats = ret["categories"]
            for cat in list(cats.keys()):
                if "_0b_" in cat or cat.endswith("_fr"):
                    del cats[cat]
            yield ret


class DYCount(Base, Histogramer):
    zveto_cuts = {"zee_mass_window_cut", "zmumu_mass_window_cut"}
    debug_dataset = "TTTo2L2Nu"
    # empty chunk after lumimask
    # debug_dataset = "data_A_e+ee"
    # debug_uuids = {"069C75AA-4E7B-1D49-A278-B683907E690B"}
    memory = "3000MiB"

    jes_shifts = False
    dataset_shifts = False
    individual_weights = True

    def variable_full_shifts(self, variable):
        return False

    @classmethod
    def requires(cls, task):
        return task.base_requires(dyestimation=False)

    @property
    def variables(self):
        return [
            v
            for v in self.analysis_inst.variables.values
            if v.name
            in (
                "jet1_pt",
                "fatjet1_msoftdrop",
                "HT",
                "MET",
                "dilep_deltaR",
            )
        ]

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            cats = ret["categories"]

            # remove z-peak cuts
            for cuts in cats.values():
                for rm in self.zveto_cuts:
                    for r in {rm, f"!{rm}"}:
                        if r in cuts:
                            cuts.remove(r)

            # skip categories
            for cat in list(cats.keys()):
                if cat.endswith("_fr"):
                    del cats[cat]

            # inject zpeak variable
            sel = ret["selection"]
            sel.add("zpeak", ~sel.all(*self.zveto_cuts))

            # build, z-veto/-peak categories (& eject others)
            for cat, cat_cuts in list(cats.items()):
                for name, z_cuts in dict(veto=list(self.zveto_cuts), peak=["zpeak"]).items():
                    cats[f"{cat}_z{name}"] = cat_cuts + z_cuts
                del cats[cat]

            # add inclusive ll-categories (ee+mumu)
            sel.add("ch_ll", sel.any("ch_ee", "ch_mumu"))
            ee_cats = list(filter(lambda c: c.startswith("ee_"), cats.keys())).copy()
            for cat in ee_cats:
                cuts = cats[cat].copy()
                cuts = list(map({"ch_ee": "ch_ll"}.get, cuts, cuts))
                cats[cat.replace("ee_", "ll_", 1)] = cuts

            w_old = ret["weights"]
            w_new = Weights(ret["n_events"])
            w_new.add(
                "all",
                w_old.partial_weight(
                    exclude=[
                        w for w in w_old._weights.keys() if "_DYest_" in w or w.startswith("fake_")
                    ],
                ),
            )
            ret["weights"] = w_new

            yield ret


class SyncSelectionExporter(Base, MCOnly, TreeExporter):
    individual_weights = True
    jes_shifts = False
    skip_cats = set()

    output = "sync_fixedSubjetBtagSF.root"

    debug_dataset = "GluGluToHHTo2B2VTo2L2Nu_node_cHHH1"
    debug_uuids = {
        "37B88CB0-5027-9344-A5BD-80A1A6998FA7",  # 2016
        "983A0436-48F1-9843-BA52-CF0FDD252EF0",  # 2017
        "E726B23B-4179-7042-8570-457C445C51FE",  # 2018
    }

    groups = {
        "resolved": "resolved_[12]b",
        "incl": "boosted_1b|resolved_[12]b",
        "_all_": "_(ee|emu|mumu)_",
        None: "inclusive",
    }
    weights = ["PDFSet_off", "PDFSet_rel", "PSWeight_ISR", "PSWeight_FSR", "ScaleWeight_Fact", "ScaleWeight_Renorm", "ScaleWeight_Mixed", "ScaleWeight_Envelope", "gen_weight", "pileup", "electron_id_loose", "electron_tth_tight", "electron_tth_relaxed", "muon_idiso_loose", "muon_tth_tight", "muon_tth_relaxed", "trigger_ee_sf", "trigger_emu_sf", "trigger_mumu_sf", "btagWeight_subjet", "btagWeight", "btagWeight_lf", "btagWeight_lfstats1", "btagWeight_lfstats2", "btagWeight_hf", "btagWeight_hfstats1", "btagWeight_hfstats2", "btagWeight_cferr1", "btagWeight_cferr2", "btagNorm", "jet_PUid_efficiency", "jet_PUid_mistag", "fake_electrons", "fake_electrons_non_closure", "fake_muons", "fake_muons_non_closure", "fake_two_non_tight", "ll_DYest_fat", "ll_DYest_jet1b", "ll_DYest_jet2b"]  # fmt: skip

    def arrays(self, X):
        out = super().arrays(X)
        for sel in X["selection"]._names:
            selection = X["selection"].require(**{sel: True})
            out[f"selection_{sel}"] = selection
        return out

    @property
    def tensors(self):
        tensors = super().tensors
        if not isinstance(tensors, dict):
            tensors = tensors.fget(self)
        tensors["weights"] = ("weights", 0, self.weights, np.float32, {})
        return tensors

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            # remove GGF signal datasets
            ret["datasets"] = {None: 1}

            # add new selection bit for easier sync
            sel = ret["selection"]
            sel.add(
                "sync",
                sel.all(*ret["common"])
                & (
                    sel.all(*ret["dim1"]["ee"])
                    | sel.all(*ret["dim1"]["emu"])
                    | sel.all(*ret["dim1"]["mumu"])
                )
                & (
                    sel.all(*ret["dim2"]["resolved_1b"])
                    | sel.all(*ret["dim2"]["resolved_2b"])
                    | sel.all(*ret["dim2"]["boosted_1b"])
                ),
            )

            # merge weights
            w = ret["weights"]
            ret["lepton_id_loose"] = (
                w._weights["electron_id_loose"] * w._weights["muon_idiso_loose"]
            )
            ret["lepton_tth_tight"] = (
                w._weights["electron_tth_tight"] * w._weights["muon_tth_tight"]
            )
            ret["lepton_tth_relaxed"] = (
                w._weights["electron_tth_relaxed"] * w._weights["muon_tth_relaxed"]
            )
            ret["total_weight"] = w.weight()
            yield ret

    def categories(self, select_output):
        selection = select_output.get("selection")
        return {"sync": selection.all("sync")}


class SyncExporter(DNNBase, SyncSelectionExporter):
    output = "sync_01.root"

    @property
    def tensors(self):
        tensors = super().tensors
        if not isinstance(tensors, dict):
            tensors = tensors.fget(self)
        tensors["pred"] = (
            "pred",
            0,
            [
                "pred_HH",
                "pred_VBF",
                "pred_H",
                "pred_DY",
                "pred_ST",
                "pred_TT",
                "pred_TTVX",
                "pred_VV(V)",
                "pred_Rare",
            ],
            np.float32,
            {},
        )
        return tensors

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            for k in list(ret["categories"].keys()):
                if not "dnn_node_" in k:
                    ret["categories"].pop(k)
            yield ret


class OverlapStudy(Exporter):
    skip_cats = {"resolved", "boosted"}
    local = "/net/scratch/cms/dihiggs/store/bbww_dl/Run2_pp_13TeV_2016/OverlapStudy/bbZZ"
    file_id = "D749F311-6C3C-D741-8E87-093404770708"
    debug_paths = [
        f"lfn:/store/mc/RunIISummer16NanoAODv7/GluGluToHHTo2B2ZTo2L2J_node_SM_TuneCUETP8M1_PSWeights_13TeV-madgraph-pythia8/NANOAODSIM/PUMoriond17_Nano02Apr2020_102X_mcRun2_asymptotic_v8-v1/110000/{file_id}.root",
    ]
    channels = ["ee", "emu", "mumu"]
    n_events = 160197

    def postprocess(self, accumulator):
        import json
        from utils.util import NumpyEncoder

        arr = accumulator["arrays"]
        n_events = list(accumulator["n_events"].items())[-1][-1]
        out = {}
        for region in ["sr", "fr"]:
            out[region] = sorted(
                list(
                    np.concatenate(
                        [
                            arr[f"{channel}_inclusive_{region}"]["eventnr"].value[:, 0]
                            for channel in self.channels
                        ]
                    )
                )
            )
            print(f"Overlap {region} = {len(out[region])/n_events * 100:.2f} %")
        with open(f"{self.local}/{self.file_id}.json", "w") as outfile:
            json.dump(out, outfile, cls=NumpyEncoder)


class NeutrinoExporter(Base, ArrayExporter):
    debug = True
    debug_dataset = "GluGluToHHTo2B2VTo2L2Nu_node_cHHH1"  # "TTTo2L2Nu"
    debug_uuids = {
        "077E69A4-6F5C-9345-BDA9-DC7F1C6D2B58",
        "4323CDDB-5915-2946-933D-97F1A5398B72",
        "76393C3D-DC57-6240-9775-EC814C91AA03",
        "983A0436-48F1-9843-BA52-CF0FDD252EF0",
        "B8178866-6856-D946-9D06-30FC3FC730EB",
        "E232CF32-97BE-B14B-B9D7-534260DC5701",
        "0A72B12D-2368-634E-91A7-373B38F30421",
        "50CBD1CA-3053-0447-991B-DA59F432F8F3",
        "7704035F-A769-184C-9370-1BF56991EB92",
        "A0C8E1F5-D23F-684F-B486-4FC3818EA737",
        "BDB05A38-87DE-5E45-8CCC-97C29EF52B2C",
        "E42587DA-1E1B-CA44-A079-178CE74B4DD0",
        "11749000-B9FC-5F48-85B9-4AA880E431B4",
        "559E53FA-C60D-B94C-8389-26B7D6D86AE1",
        "79856C43-2C36-064D-8525-7DA8302FB177",
        "A30C8854-4E5C-8F49-9347-82D92FFA80A0",
        "C3EA34B8-749C-C84E-B288-0DA589519F43",
        "F7EB0962-2335-1A42-85B7-CB459E8CA0B2",
        "16C02C80-CB52-5C42-AF79-7991632D261C",
        "561BE427-CB95-8945-82B3-F159186CAB65",
        "7A11DD0D-1020-8848-AAEA-40F00BE359D8",
        "A70EEC77-827F-884A-A7CC-02A2118DFCC5",
        "C692C3A4-1B2D-EE4E-8739-939F2C367BFE",
        "F8208C4A-6F37-944B-BF11-587962CA7777",
        "16E3DD00-463A-574D-8AE3-C88073751374",
        "57469B83-C1DB-4A4C-A9E4-23D91EEF517C",
        "7D9F25DA-D167-A745-BD1F-4BACB77F329D",
        "A87DF20B-6750-C647-8ADB-317DFFA08851",
        "CD91FD58-3581-3B4F-ADC3-7EDAB3A55B66",
        "F8FFCC73-FBA1-E743-BEC7-43F517A7B5B0",
        "18B87E42-F304-3B4D-B88A-063C5DEEFCA6",
        "5D95C076-6CE3-3845-B2AA-F1A83DD14F39",
        "821B5333-523C-3142-B9E6-32644019E470",
        "B2760BDD-FA4D-B54B-A720-5686163FA8BE",
        "CE71D951-03E7-BC40-BF48-7C675C86AFEE",
        "FCAAA923-3535-E746-9D0C-F2408055ED45",
        "1F17A42F-EFC0-7748-A55D-B884F277A3A5",
        "65ACA939-CECD-4443-AE41-88DF6725D44F",
        "8343B61F-2A02-2941-977C-35C1CAD379CE",
        "B5617F0A-51DF-934B-801F-D96C242D388A",
        "D2CF17B4-20DC-3543-8C63-50EE731AA303",
        "3F21FBEC-E31A-3346-BA1A-0AE1EE11A33C",
        "66442438-D46D-6649-AF02-C8B697E0ECC2",
        "89E8C1D8-9F44-4B4D-A845-143DFDF7D28E",
        "B5D3C164-9CB3-B24A-808D-0CCE26E7825A",
        "D4AA4D46-B53A-A944-BCEE-ACD2EB79DBA4",
    }

    def categories(self, select_output):
        selection = select_output.get("selection")
        categories = select_output.get("categories")
        return {
            "mumu": (
                selection.all(*(categories["mumu_resolved_2b_sr"] + ["n_nugen"]))
                | selection.all(*(categories["mumu_resolved_1b_sr"] + ["n_nugen"]))
            )
        }

    def arrays(self, select_output):
        return {
            "dinugen_m": select_output.get("dinugen_m"),
            "dinugen_e": select_output.get("dinugen_e"),
            "dinugen_x": select_output.get("dinugen_x"),
            "dinugen_y": select_output.get("dinugen_y"),
            "dinugen_z": select_output.get("dinugen_z"),
            "dimu_m": select_output.get("dimu_m"),
            "dimu_e": select_output.get("dimu_e"),
            "dimu_x": select_output.get("dimu_x"),
            "dimu_y": select_output.get("dimu_y"),
            "dimu_z": select_output.get("dimu_z"),
            "met_e": select_output.get("met_e"),
            "met_x": select_output.get("met_x"),
            "met_y": select_output.get("met_y"),
            "met_z": select_output.get("met_z"),
        }

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            # muon neutrino selection:
            gen = ret["events"].GenPart
            hgen = gen[gen.hasFlags(["isHardProcess", "isFirstCopy"])]
            nuhgen = hgen[abs(hgen.pdgId) == 14]
            nuhgen_withW = nuhgen[abs(nuhgen.distinctParent.pdgId) == 24]

            ret["selection"].add(
                "n_nugen",
                ak.num(nuhgen_withW) == 2,
            )

            # di-(muon)neutrino system
            dinugen = nuhgen_withW
            ret["dinugen_m"] = util.normalize(dinugen.sum().mass)
            ret["dinugen_e"] = util.normalize(dinugen.sum().energy)
            ret["dinugen_x"] = util.normalize(dinugen.sum().x)
            ret["dinugen_y"] = util.normalize(dinugen.sum().y)
            ret["dinugen_z"] = util.normalize(dinugen.sum().z)

            # di-muon system
            good_muons = ret["good_muons"]
            ret["dimu_m"] = util.normalize(good_muons.sum().mass)
            ret["dimu_e"] = util.normalize(good_muons.sum().energy)
            ret["dimu_x"] = util.normalize(good_muons.sum().x)
            ret["dimu_y"] = util.normalize(good_muons.sum().y)
            ret["dimu_z"] = util.normalize(good_muons.sum().z)

            # MET
            metp4 = ret["metp4"]
            ret["met_e"] = util.normalize(metp4.energy)
            ret["met_x"] = util.normalize(metp4.x)
            ret["met_y"] = util.normalize(metp4.y)
            ret["met_z"] = util.normalize(metp4.z)

            yield ret


class TriggerPresel(BaseProcessor):
    debug_dataset = "GluGluToHHTo2B2VTo2L2Nu_node_cHHH1"
    trigger = Base.trigger

    @classmethod
    def requires(cls, task):
        return task.base_requires(corrections=False)

    def __init__(self, task):
        super().__init__(task)

        self._accumulator = dict_accumulator(
            n_events_before=defaultdict_accumulator(int),
            n_events_after=defaultdict_accumulator(int),
        )

    def process(self, events):
        output = self.accumulator.identity()
        dataset_key = tuple(events.metadata["dataset"])
        tr = []
        for k in self.trigger.values():
            tr.extend(list(k.keys()))
        m = util.nano_mask_or(events.HLT, tr, skipable=True)
        output["n_events_before"]["inclusive"] += len(events)
        output["n_events_before"][dataset_key] += len(events)
        output["n_events_after"]["inclusive"] += np.sum(m)
        output["n_events_after"][dataset_key] += np.sum(m)
        return output
